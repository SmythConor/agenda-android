package co.agendaapp.registration;

import android.view.View;
import android.os.Bundle;
import android.widget.Toast;
import android.app.Activity;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.agendaapp.R;
import co.agendaapp.model.User;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.cache.CacheFileNames;
import co.agendaapp.utils.ResponseCleaner;
import co.agendaapp.service.HttpRequestTask;

import static android.widget.Toast.LENGTH_SHORT;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;
import static co.agendaapp.constants.ServiceConstants.USER_DOES_NOT_EXIST;
import static co.agendaapp.constants.ValidationConstants.BLANK_EMAIL;
import static co.agendaapp.constants.ValidationConstants.LOG_IN_SUCCESS;
import static co.agendaapp.constants.ValidationConstants.NOT_VERIFIED;
import static co.agendaapp.constants.ValidationConstants.PASSWORD_INCORRECT;
import static co.agendaapp.constants.ValidationConstants.STRING_EMPTY;
import static co.agendaapp.constants.ValidationConstants.BLANK_PASSWORD;

/**
 * Home class for the main log in page
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-01-31
 */
public class SignIn extends Activity implements HttpRequestTask.ResultListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_in);

		final Button signUpButton = (Button) findViewById(R.id.signUpButton);
		signUpButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				final Intent registerScreen = new Intent(getApplicationContext(), SignUp.class);

				startActivity(registerScreen);
			}
		});

		final Button logInButton = (Button) findViewById(R.id.logInButton);
		logInButton.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(validateInput()) {
					logInAction();
				}
			}
		});
	}

	/**
	 * Log in
	 */
	private void logInAction() {
		final String URI = "/logIn";

		User user = getUserDetails();

		new HttpRequestTask(URI, HttpMethod.POST, SignIn.this).execute(user);
	}

	private boolean validateInput() {
		String email = getFieldText(R.id.emailField);
		String password = getFieldText(R.id.passwordField);

		return checkBlanks(new String[]{email, password});
	}

	private boolean checkBlanks(String[] fieldValues) {
		String email = fieldValues[0];

		if(email.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_EMAIL, LENGTH_SHORT).show();

			return false;
		}

		String password = fieldValues[1];

		if(password.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_PASSWORD, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Get the user details from the fields
	 * @return the user details
	 */
	private User getUserDetails() {
		String email = getFieldText(R.id.emailField).toLowerCase();
		String password = getFieldText(R.id.passwordField);

		return new User(email, password);
	}

	/**
	 * Get the text from an edit text field
	 * @param id the id of the field
	 * @return the text from the field as a string
	 */
	private String getFieldText(int id) {
		return ((EditText) findViewById(id)).getText().toString().trim();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String uri) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;

		if(response != null) {
			HttpStatus status = response.getStatusCode();

			if(status.equals(HttpStatus.OK)) {
				Object temp = response.getBody();
				User body = ResponseCleaner.cleanUser(temp);

				CacheHelper.write(getCacheDir(), CacheFileNames.USER_CACHE_NAME, body);

				Toast.makeText(getApplicationContext(), LOG_IN_SUCCESS, LENGTH_SHORT).show();

				finish();
			} else if(status.equals(HttpStatus.UNPROCESSABLE_ENTITY)) {
				((EditText) findViewById(R.id.passwordField)).setText(STRING_EMPTY);
				Toast.makeText(getApplicationContext(), PASSWORD_INCORRECT, LENGTH_SHORT).show();
			} else if(status.equals(HttpStatus.UNAUTHORIZED)) {
				Toast.makeText(getApplicationContext(), NOT_VERIFIED, LENGTH_SHORT).show();
			} else if(status.equals(HttpStatus.NOT_FOUND)) {
                Toast.makeText(getApplicationContext(), USER_DOES_NOT_EXIST, LENGTH_SHORT).show();
            } else {
				Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
			}
		}
	}
}