package co.agendaapp.registration;

import android.os.Bundle;
import android.view.View;
import android.app.Activity;
import android.widget.Toast;
import android.widget.Button;
import android.widget.EditText;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.regex.Pattern;

import co.agendaapp.R;
import co.agendaapp.cache.CacheFileNames;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.model.User;
import co.agendaapp.model.Verified;
import co.agendaapp.encrypt.Encryptor;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.encrypt.EncryptionUtils;

import static android.widget.Toast.LENGTH_SHORT;
import static co.agendaapp.constants.ValidationConstants.*;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;

/**
 * Class for registration of a user
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-01-31
 */
public class SignUp extends Activity implements HttpRequestTask.ResultListener {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_sign_up);

		final Button button = (Button) findViewById(R.id.signUpButton);
		button.setOnClickListener(new View.OnClickListener() {
			public void onClick(View v) {
				if(validateInput()) {
					signUpAction();
				}
			}
		});
	}

	/**
	 * Generate salt, encrypt password and communicate with server
	 */
	private void signUpAction() {
		User user = getUserDetails();

		byte[] bytePassword = EncryptionUtils.stringToBytes(user.getPassword());
		byte[] salt = EncryptionUtils.getSalt();

		String hashedPassword = Encryptor.hashPassword(bytePassword, salt);
		String saltS = EncryptionUtils.bytesToHex(salt);

		String registrationId = (String) CacheHelper.read(getCacheDir(), CacheFileNames.REG_ID_CACHE_NAME, String.class);

		user.setPassword(hashedPassword);
		user.setSalt(saltS);
		user.setIsVerified(Verified.NOT);
		user.setRegistrationId(registrationId);

		final String URI = "/signUp/register";

		new HttpRequestTask(URI, HttpMethod.POST, SignUp.this).execute(user);
	}

	/**
	 * Get the user details
	 * @return user object with field values
	 */
	private User getUserDetails() {
		String[] fieldValues = getFieldValues();

		String name = fieldValues[NAME_INDEX];
		String email = fieldValues[EMAIL_INDEX];
		String password = fieldValues[PASSWORD_INDEX];

		User user = new User();

		user.setName(name);
		user.setEmail(email);
		user.setPassword(password);

		return user;
	}

	/**
	 * Validate the input for the fields
	 * @return true if all fields are valid
	 */
	private boolean validateInput() {
		String[] fieldValues = getFieldValues();

		return checkBlanks(fieldValues)
				&& checkLengths(fieldValues)
				&& checkEmail(fieldValues)
				&& checkPassword(fieldValues);
	}

	/**
	 * Check all the fields for blanks
	 * @param fieldValues the field values
	 * @return true if none of the fields aren't blank
	 */
	private boolean checkBlanks(String[] fieldValues) {
		String name = fieldValues[NAME_INDEX];

		if(name.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_NAME, LENGTH_SHORT).show();

			return false;
		}

		String email = fieldValues[EMAIL_INDEX];

		if(email.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_EMAIL, LENGTH_SHORT).show();

			return false;
		}

		String password = fieldValues[PASSWORD_INDEX];

		if(password.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_PASSWORD, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Check the lengths of all the fields
	 * @param fieldValues the field values
	 * @return true if the lengths are good
	 */
	private boolean checkLengths(String[] fieldValues) {
		String name = fieldValues[NAME_INDEX];

		if(name.length() > MAX_LENGTH_NAME) {
			Toast.makeText(getApplicationContext(), NAME_TOO_LONG, LENGTH_SHORT).show();

			return false;
		}

		String email = fieldValues[EMAIL_INDEX];
		Integer emailLength = email.length();

		if(emailLength < MIN_LENGTH_EMAIL || emailLength > MAX_LENGTH_EMAIL) {
			Toast.makeText(getApplicationContext(), EMAIL_LENGTH_ERR, LENGTH_SHORT).show();

			return false;
		}

		String password = fieldValues[PASSWORD_INDEX];
		Integer passwordLength = password.length();

		if(passwordLength < MIN_LENGTH_PASSWORD || passwordLength > MAX_LENGTH_PASSWORD) {
			Toast.makeText(getApplicationContext(), PASSWORD_LENGTH_ERR, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Check the email address is valid format
	 * @param fieldValues the field values
	 * @return true if the email is in the correct format
	 */
	private boolean checkEmail(String[] fieldValues) {
		String email = fieldValues[EMAIL_INDEX];

		if(!Pattern.matches(EMAIL_REGEX, email)) {
			Toast.makeText(getApplicationContext(), INVALID_EMAIL, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Check the password fields to see if they match
	 * @param fieldValues the field values
	 * @return true if the passwords match
	 */
	private boolean checkPassword(String[] fieldValues) {
		String password = fieldValues[PASSWORD_INDEX];
		String reenterPassword = fieldValues[RE_PASSWORD_INDEX];

		if(!password.equals(reenterPassword)) {
			Toast.makeText(getApplicationContext(), PASSWORD_MISMATCH, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Get the values from all the fields
	 * @return an array of the field values
	 */
	private String[] getFieldValues() {
		String name = getFieldText(R.id.nameField);
		String email = getFieldText(R.id.emailField).toLowerCase();
		String password = getFieldText(R.id.passwordField);
		String reenterPassword = getFieldText(R.id.re_enterPasswordField);

		String[] fieldValues = new String[FIELD_SIZE];

		fieldValues[NAME_INDEX] = name;
		fieldValues[EMAIL_INDEX] = email;
		fieldValues[PASSWORD_INDEX] = password;
		fieldValues[RE_PASSWORD_INDEX] = reenterPassword;

		return fieldValues;
	}

	/**
	 * Get the text from a specific field
	 * @param id the id of the field
	 * @return the value from that field as a string and trimmed
	 */
	private String getFieldText(int id) {
		return ((EditText) findViewById(id)).getText().toString().trim();
	}

	/**
	 * Function to listen for result received from the server
	 * @param activity this activity
	 * @param value the value to get from the request
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String uri) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;

		if(response != null) {
			HttpStatus status = response.getStatusCode();

			if(status.equals(HttpStatus.CREATED)) {
				Toast.makeText(getApplicationContext(), SIGN_UP_SUCCESS, LENGTH_SHORT).show();

				finish();
			} else if(status.equals(HttpStatus.UNPROCESSABLE_ENTITY)) {
				Toast.makeText(getApplicationContext(), DUPLICATE_EMAIL, LENGTH_SHORT).show();
			} else {
				Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
			}
		}
	}
}