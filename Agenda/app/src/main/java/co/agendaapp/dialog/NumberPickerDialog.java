package co.agendaapp.dialog;

import android.os.Bundle;
import android.app.Dialog;
import android.app.Activity;
import android.app.DialogFragment;
import android.widget.NumberPicker;
import android.content.DialogInterface;
import android.app.AlertDialog;

import co.agendaapp.R;

/**
 * Dialog for number picker
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-29
 */
public class NumberPickerDialog extends DialogFragment {
	private NumberPickerDialogListener listener;
	private Integer openValue;

	public void setOpenValue(Integer openValue) {
		this.openValue = openValue;
	}

	public interface NumberPickerDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog, String value);
	}

	public Dialog onCreateDialog(final Bundle savedBundleInstance) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		final NumberPicker numberPicker = new NumberPicker(getActivity().getApplicationContext());

		numberPicker.setMinValue(0);
		numberPicker.setMaxValue(365);
		numberPicker.setValue(openValue);
		numberPicker.setBackgroundResource(R.color.colorPrimary);

		builder.setView(numberPicker);
		builder.setMessage("Pick a number").setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				Integer numberPicked = numberPicker.getValue();

				listener.onDialogPositiveClick(NumberPickerDialog.this, String.valueOf(numberPicked));
			}
		}).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dismiss();
			}
		});

		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			listener = (NumberPickerDialogListener) activity;
		} catch(ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement NumberPickerDialogListener");
		}
	}
}
