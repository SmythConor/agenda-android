package co.agendaapp.dialog;

import android.os.Bundle;
import android.app.Dialog;
import android.widget.Toast;
import android.app.Activity;
import android.graphics.Color;
import android.text.InputType;
import android.widget.EditText;
import android.app.AlertDialog;
import android.app.DialogFragment;
import android.content.DialogInterface;

import static co.agendaapp.constants.DialogConstants.CANCEL;
import static co.agendaapp.constants.ValidationConstants.STRING_EMPTY;

/**
 * General Purpose dialog for Single field input field
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-03
 */
public class AddCreateDialog extends DialogFragment {
	private String hint;
	private String message;
	private String positiveButton;

	AddMemberDialogListener mListener;

	public void setHint(String hint) {
		this.hint = hint;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setPositiveButton(String positiveButton) {
		this.positiveButton = positiveButton;
	}

	public interface AddMemberDialogListener {
		public void onDialogPositiveClick(DialogFragment dialog, String value);
	}

	@Override
	public Dialog onCreateDialog(final Bundle savedInstanceState) {
		final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		final EditText input = new EditText(getActivity().getApplicationContext());

		input.setTextColor(Color.BLACK);
		input.setHintTextColor(Color.GRAY);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setHint(hint);

		builder.setView(input);
		builder.setMessage(message).setPositiveButton(positiveButton, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				String email = input.getText().toString().trim();

				if(!isBlank(email)) {
					mListener.onDialogPositiveClick(AddCreateDialog.this, email);
				} else {
					Toast.makeText(getActivity().getApplicationContext(), "Field cannot be blank", Toast.LENGTH_SHORT).show();
				}
			}
		}).setNegativeButton(CANCEL, new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dismiss();
			}
		});

		return builder.create();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		try {
			mListener = (AddMemberDialogListener) activity;
		} catch(ClassCastException e) {
			throw new ClassCastException(activity.toString() + " must implement AddMemberDialogListener");
		}
	}

	private boolean isBlank(String str) {
		return str.equals(STRING_EMPTY);
	}
}