package co.agendaapp.messaging;

import android.content.Intent;

/**
 * Handle Token updating
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-15
 */
public class InstanceIDListenerService extends com.google.android.gms.iid.InstanceIDListenerService {

	@Override
	public void onTokenRefresh() {
		super.onTokenRefresh();

		Intent registrationIntentService = new Intent(this, RegistrationIntentService.class);
		startService(registrationIntentService);
	}
}