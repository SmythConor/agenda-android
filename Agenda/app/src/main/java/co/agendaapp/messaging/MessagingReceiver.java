package co.agendaapp.messaging;

import android.content.Intent;
import android.content.Context;

import com.google.android.gms.gcm.GcmReceiver;

/**
 * Receiver for the GCM messages
 * Triggers the GcmListenerService to handle the message
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-22
 */
public class MessagingReceiver extends GcmReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		super.onReceive(context, intent);
	}
}
