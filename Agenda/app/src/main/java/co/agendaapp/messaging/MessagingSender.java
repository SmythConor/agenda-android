package co.agendaapp.messaging;

import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.gcm.GoogleCloudMessaging;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.Scanner;

import co.agendaapp.constants.Tag;
import co.agendaapp.model.Message;
import co.agendaapp.model.User;

import static co.agendaapp.constants.ServiceConstants.GCM_AUTH_KEY;
import static co.agendaapp.constants.ServiceConstants.GCM_CREATE_URL;
import static co.agendaapp.constants.ServiceConstants.GCM_DATA;
import static co.agendaapp.constants.ServiceConstants.GCM_MESSAGE;
import static co.agendaapp.constants.ServiceConstants.GCM_NOTIFICATION_KEY;
import static co.agendaapp.constants.ServiceConstants.GCM_NOTIFICATION_KEY_NAME;
import static co.agendaapp.constants.ServiceConstants.GCM_OPERATION;
import static co.agendaapp.constants.ServiceConstants.GCM_PROJECT_ID;
import static co.agendaapp.constants.ServiceConstants.GCM_REG_IDS;
import static co.agendaapp.constants.ServiceConstants.GCM_SEND;
import static co.agendaapp.constants.ServiceConstants.GCM_TO;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;

/**
 * Aysnc task to send a message
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-22
 */
public class MessagingSender extends AsyncTask<Void, Void, Void> {
	private URL url;
	private HttpURLConnection httpURLConnection;

	private String noticiationKey;
	private Message message;
	private User user;

	public MessagingSender(String notificationKey, Message message, User user) {
		this.noticiationKey = notificationKey;
		this.message = message;
		this.user = user;
	}

	@Override
	protected Void doInBackground(Void... params) {
			try {
				this.url = new URL(GCM_SEND);
			} catch(MalformedURLException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

		/* open connection */
			try {
				this.httpURLConnection = (HttpURLConnection) url.openConnection();
			} catch(IOException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

		/* Set request method */
			try {
				this.httpURLConnection.setRequestMethod("POST");
			} catch(ProtocolException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

		/* Set up headers */
			this.httpURLConnection.setDoOutput(true);
			this.httpURLConnection.setDoInput(true);
			this.httpURLConnection.setRequestProperty("Content-Type", "application/json");
			this.httpURLConnection.setRequestProperty("Authorization", GCM_AUTH_KEY);

		/* Connect */
			try {
				this.httpURLConnection.connect();
			} catch(IOException e) {
				Log.e(Tag.TAG, e.getMessage());
			}


		/* Create json body and add fields */
			JSONObject requestBody = new JSONObject();
			try {
                /* Construct the message body */
                JSONObject messageBody = new JSONObject();

                messageBody.put("messageId", this.message.getMessageId());
                messageBody.put("messageBody", this.message.getMessageBody());
                messageBody.put("senderId", this.message.getSenderId());
                messageBody.put("groupId", this.message.getGroupId());
                messageBody.put("sentTimestamp", this.message.getSentTimestamp());

                /* construct the messgae */
				JSONObject message = new JSONObject();

				message.put(GCM_MESSAGE, messageBody);
				message.put("messageId", this.message.getMessageId());
				message.put("author", user.getName());

                /* Construct the complete request body */
				requestBody.put(GCM_DATA, message);
				requestBody.put(GCM_TO, this.noticiationKey);
				requestBody.put("priority", "high");
			} catch(JSONException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

		/* Write the json object to connection */
			try {
				OutputStream os = this.httpURLConnection.getOutputStream();
				os.write(requestBody.toString().getBytes("UTF-8"));
				os.close();
			} catch(IOException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

			try {
				InputStream is;

				if(this.httpURLConnection.getResponseCode() != 200) {
				/* if not 200, log error and return error */
					is = this.httpURLConnection.getErrorStream();

					String response = new Scanner(is, "UTF-8").useDelimiter("\\A").next();

					Log.e(Tag.TAG, response);

					is.close();
				} else {
				/* if is 200, extract key and return */
					is = this.httpURLConnection.getInputStream();

					String response = new Scanner(is, "UTF-8").useDelimiter("\\A").next();

					Log.i(Tag.TAG, response);
				}
			} catch(IOException e) {
				Log.e(Tag.TAG, e.getMessage());
			}

		return null;
	}
}
