package co.agendaapp.messaging;

import android.util.Log;
import android.content.Intent;
import android.app.IntentService;

import com.google.android.gms.iid.InstanceID;
import com.google.android.gms.gcm.GoogleCloudMessaging;

import co.agendaapp.R;
import co.agendaapp.constants.Tag;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.cache.CacheFileNames;

public class RegistrationIntentService extends IntentService {

	public RegistrationIntentService() {
		super(Tag.TAG);
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		try {
			/* Get token from GCM */
			InstanceID instanceID = InstanceID.getInstance(this);
			String token = instanceID.getToken(getString(R.string.gcm_defaultSenderId),
					GoogleCloudMessaging.INSTANCE_ID_SCOPE);

			Log.i(Tag.TAG, "GCM Registration Token: " + token);

			writeToken(token);
		} catch(Exception e) {
			Log.d(Tag.TAG, "Failed to complete token refresh", e);
		}
	}

	/**
	 * Write the token to cache for later
	 * @param token The new token.
	 */
	private void writeToken(String token) {
		CacheHelper.write(getCacheDir(), CacheFileNames.REG_ID_CACHE_NAME, token);
	}
}