package co.agendaapp.messaging;

import android.app.Activity;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.NotificationCompat;
import android.support.v4.app.NotificationCompat.Builder;
import android.util.Log;

import com.fasterxml.jackson.core.JsonProcessingException;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;

import co.agendaapp.R;
import co.agendaapp.agenda.Home;
import co.agendaapp.constants.Tag;
import co.agendaapp.group.GroupScreen;
import co.agendaapp.model.Message;

import static co.agendaapp.constants.ServiceConstants.GCM_MESSAGE;

/**
 * Listener service to handle receiving a message
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-15
 */
public class GcmListenerService extends com.google.android.gms.gcm.GcmListenerService {
	private static ResultNotifier resultNotifier;

	public interface ResultNotifier {
		public void onResultReceived(Object message, String author);
	}

	public static void setMessagingScreen(Fragment fragment) {
		resultNotifier = (MessagingScreen) fragment;
	}

	@Override
	public void onSendError(String msgId, String error) {
		super.onSendError(msgId, error);
		Log.e(Tag.TAG, msgId + error);
	}

	@Override
	public void onMessageReceived(String from, Bundle data) {
		super.onMessageReceived(from, data);

		Log.i(Tag.TAG, "from: " + from);


        String tempMessageBody = (String) data.get(GCM_MESSAGE);
        JSONObject messageBody = null;
        try {
            messageBody = new JSONObject(tempMessageBody);
        } catch(JSONException e) {
            Log.e(Tag.TAG, e.getMessage());
        }
        String author = (String) data.get("author");

        Message message = new Message();

        if(messageBody != null) {
            try {
                message.setMessageId((Integer) messageBody.get("messageId"));
                message.setMessageBody((String) messageBody.get("messageBody"));
                message.setGroupId((Integer) messageBody.get("groupId"));
                message.setSenderId((Integer) messageBody.get("senderId"));
                message.setSentTimestamp(new DateTime(messageBody.get("sentTimestamp")));
            } catch(JSONException e) {
                Log.e(Tag.TAG, e.getMessage());
            }
        }

        message.setDeliveredTimestamp(DateTime.now());

		resultNotifier.onResultReceived(message, author);

        //sendNotification(message);
	}

	@Override
	public void onMessageSent(String msgId) {
        super.onMessageSent(msgId);
	}

	private void sendNotification(String message) {
		Intent intent = new Intent(this, Home.class);
		intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
		PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT);

		Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
		Builder notificationBuilder = new NotificationCompat.Builder(this)
				.setSmallIcon(R.mipmap.ic_launcher)
				.setContentTitle("GCM Message")
				.setContentText(message)
				.setAutoCancel(true).setSound(defaultSoundUri)
				.setContentIntent(pendingIntent);

		NotificationManager notificationManager =
				(NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

		notificationManager.notify(0, notificationBuilder.build());
	}
}