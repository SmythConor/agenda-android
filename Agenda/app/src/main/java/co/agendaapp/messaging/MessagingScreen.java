package co.agendaapp.messaging;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.android.gms.gcm.*;

import org.joda.time.DateTime;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import co.agendaapp.R;
import co.agendaapp.adapter.MessageListAdapter;
import co.agendaapp.cache.CacheFileNames;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.constants.Tag;
import co.agendaapp.group.GroupScreen;
import co.agendaapp.model.Group;
import co.agendaapp.model.Message;
import co.agendaapp.model.User;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.utils.MessageComparator;

import static co.agendaapp.constants.BundleConstants.GROUP;
import static co.agendaapp.constants.BundleConstants.MESSAGE;
import static co.agendaapp.constants.ServiceConstants.MESSAGE_URI;
import static co.agendaapp.helpers.BundleHelper.getSelectedGroup;

/**
 * Messaging screen for messaging functionality
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-03
 */
public class MessagingScreen extends Fragment implements GroupScreen.ResultListener, GcmListenerService.ResultNotifier {

	private View view;
	private Group selectedGroup;
	private List<Message> messages;
	private ListView messagesListView;
	private ArrayAdapter<Message> messagesAdapter;
	private User user;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		GcmListenerService.setMessagingScreen(this);

        if(messages == null) {
            messages = new LinkedList<>();
        }

		return inflater.inflate(R.layout.activity_messaging, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		user = (User) CacheHelper.read(getActivity().getCacheDir(), CacheFileNames.USER_CACHE_NAME, User.class);

		this.view = view;
		this.selectedGroup = getSelectedGroup(getArguments());
		this.messagesListView = (ListView) view.findViewById(R.id.messagesList);
		this.messagesListView.setClickable(false);
		this.messagesAdapter = new MessageListAdapter(getContext(), messages);
		this.messagesListView.setAdapter(this.messagesAdapter);
		this.messagesAdapter.setNotifyOnChange(true);
		this.messagesAdapter.notifyDataSetChanged();

		final Button sendButton = (Button) view.findViewById(R.id.sendMessageButton);
		sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
            }
        });
	}

    @Override
    public void onStart() {
        super.onStart();

        getMessages();
    }

    private void getMessages() {
        final String URI = MESSAGE_URI + "/getMessages/" + this.selectedGroup.getGroupId();

        new HttpRequestTask(URI, HttpMethod.GET, getActivity()).execute();
    }

	private void sendMessage() {
		/* Get the notification key for this group */
		String notificationKey = selectedGroup.getNotificationKey();

		/* Get the value from the field */
		String messageVal = ((EditText) view.findViewById(R.id.messageField)).getText().toString();

        Message message = new Message();
        message.setMessageId(getMsgId());
        message.setMessageBody(messageVal);
        message.setGroupId(this.selectedGroup.getGroupId());
        message.setSenderId(this.user.getUserId());
        message.setSentTimestamp(DateTime.now());


		/* Send the message to the given group */
		new MessagingSender(notificationKey, message, user).execute();

        final String URI = MESSAGE_URI + "/createMessage";
        new HttpRequestTask(URI, HttpMethod.POST, getActivity()).execute(message);
	}

	@Override
	public void onResultReceived(Object msg, String author) {
		Message message = (Message) msg;

		populateList(message);

        Log.i(Tag.TAG, "Messaging result received" + msg);
	}

	@Override
    @SuppressWarnings("unchecked")
	public void onResultReceived(Object value, Context context) {
        List<Message> messages = (List) value;

        populateList(messages);
	}

	private void populateList(final Message messsage) {
		getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                messages.add(messsage);
                messagesAdapter.notifyDataSetChanged();

                final String URI = MESSAGE_URI + "/updateMessage";

                new HttpRequestTask(URI, HttpMethod.PUT, getActivity()).execute(messsage);
            }
        });
	}

    private void populateList(List<Message> messages) {
        this.messages = messages;

        Collections.sort(this.messages, new MessageComparator());

        ArrayAdapter<Message> messageAdapter = new MessageListAdapter(getContext(), this.messages);

        this.messagesListView.setAdapter(messagesAdapter);

        messageAdapter.notifyDataSetChanged();
    }

	/**
	 * Get a new msg id
	 * @return msg id as String
	 */
	private Integer getMsgId() {
        AtomicInteger msgId = new AtomicInteger();

        if(this.messages.size() != 0) {
            msgId = new AtomicInteger(this.messages.get(messages.size() - 1).getMessageId());
        }

		return msgId.incrementAndGet();
	}
}