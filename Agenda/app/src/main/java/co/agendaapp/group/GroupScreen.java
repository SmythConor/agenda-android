package co.agendaapp.group;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.widget.Toast;
import android.app.Activity;
import android.view.MenuItem;
import android.app.ActionBar;
import android.content.Intent;
import android.view.MenuInflater;
import android.app.DialogFragment;
import android.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.support.v4.app.FragmentActivity;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.LinkedList;
import java.util.List;

import co.agendaapp.R;
import co.agendaapp.constants.Tag;
import co.agendaapp.helpers.BundleHelper;
import co.agendaapp.messaging.RegistrationIntentService;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.model.Message;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.dialog.AddCreateDialog;
import co.agendaapp.service.GcmGroupService;
import co.agendaapp.utils.ResponseCleaner;
import co.agendaapp.adapter.TabsPagerAdapter;
import co.agendaapp.service.HttpRequestTask;

import static android.widget.Toast.LENGTH_SHORT;

import static co.agendaapp.constants.ServiceConstants.CALENDAR_URI;
import static co.agendaapp.constants.ServiceConstants.GCM_GROUP_ADD;
import static co.agendaapp.constants.ServiceConstants.MESSAGE_URI;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;
import static co.agendaapp.constants.ServiceConstants.GROUP_URI;
import static co.agendaapp.constants.ServiceConstants.TO_DO_LIST_URI;
import static co.agendaapp.constants.TitleConstants.GROUP_TITLE;
import static co.agendaapp.constants.ValidationConstants.BLANK_EMAIL;
import static co.agendaapp.constants.DialogConstants.ADD_MEMBER_HINT;
import static co.agendaapp.constants.ValidationConstants.STRING_EMPTY;
import static co.agendaapp.constants.DialogConstants.ADD_MEMBER_BUTTON;
import static co.agendaapp.constants.DialogConstants.ADD_MEMBER_MESSAGE;
import static co.agendaapp.constants.ValidationConstants.USER_NOT_FOUND;
import static co.agendaapp.constants.ValidationConstants.USER_ADDED_SUCCESS;

import static org.springframework.http.HttpMethod.PUT;
import static org.springframework.http.HttpStatus.ACCEPTED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

/**
 * Group Screen class
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-02
 */
@SuppressWarnings("deprecation")
public class GroupScreen extends FragmentActivity implements AddCreateDialog
		.AddMemberDialogListener, HttpRequestTask.ResultListener, ActionBar.TabListener, ViewPager
		.OnPageChangeListener {

	private Group selectedGroup;

	private ViewPager viewPager;
	private ActionBar actionBar;
	private String[] tabs = {"Calendar", "Messaging", "To-Do List"};
	private ResultListener[] notifiers = new ResultListener[3];

	public interface ResultListener {
		public void onResultReceived(Object value, Context context);
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_screen);

		viewPager = (ViewPager) findViewById(R.id.tabPager);
		viewPager.setAdapter(new TabsPagerAdapter(getSupportFragmentManager(), getIntent().getExtras(), notifiers));

		viewPager.setOnPageChangeListener(this);

		actionBar = getActionBar();

		if(actionBar != null) {
			actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

			for(String tabName : tabs) {
				actionBar.addTab(actionBar.newTab().setText(tabName).setTabListener(this));
			}
		}

		selectedGroup = BundleHelper.getSelectedGroup(getIntent().getExtras());

		String title = GROUP_TITLE;
		if(selectedGroup != null) {
			title += selectedGroup.getName();
		}

		setTitle(title);
	}

	/**
	 * Create the menu for the settings
	 * @param menu the menu
	 * @return true
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.menu_group, menu);

		return true;
	}

	/**
	 * Handle menu selection
	 * @param item the item selected
	 * @return true
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.menu_open_settings:
				openSettings();
				return true;
			case R.id.menu_add_member:
				openAddMemberScreen();
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Open add member dialog
	 */
	private void openAddMemberScreen() {
		AddCreateDialog addCreateDialog = new AddCreateDialog();

		addCreateDialog.setHint(ADD_MEMBER_HINT);
		addCreateDialog.setMessage(ADD_MEMBER_MESSAGE);
		addCreateDialog.setPositiveButton(ADD_MEMBER_BUTTON);

		addCreateDialog.show(getFragmentManager(), ADD_MEMBER_MESSAGE);
	}

	/**
	 * Add the member
	 * @param email the email of the user to add
	 */
	private void addMember(String email) {
		final String URI = GROUP_URI + "/addMember/" + selectedGroup.getGroupId();

		User user = new User();

		user.setEmail(email);
		user.setRegistrationId(selectedGroup.getNotificationKey());

		new HttpRequestTask(URI, PUT, GroupScreen.this).execute(user);
	}

	/**
	 * Open settings screen
	 */
	private void openSettings() {
		final Intent settingsScreen = new Intent(getApplicationContext(), GroupSettings.class);

		settingsScreen.putExtras(getIntent().getExtras());

		startActivity(settingsScreen);
	}

	/**
	 * Handle dialog click
	 * @param dialog the associated dialog
	 * @param email the email entered into the dialog
	 */
	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String email) {
		if(validateField(email)) {
			addMember(email);
		}
	}

	/**
	 * Check the email field is valid
	 * @param email the email to check
	 * @return true if email text is valid
	 */
	private boolean validateField(String email) {
		if(email.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_EMAIL, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	/**
	 * Listener to handle a result from the request
	 * @param activity this activity
	 * @param value the value returned
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String uri) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;

		if(response != null) {
			if(uri.contains(GROUP_URI)) {
				groupResponse(response);
			} else if(uri.contains(CALENDAR_URI)) {
				calendarResponse(response, uri);
			} else if(uri.contains(TO_DO_LIST_URI)) {
				toDoListResponse(response, uri);
			} else if(uri.contains(MESSAGE_URI)) {
                messagingResponse(response, uri);
            }
		}
	}

	@Override
	public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
		int tabPosition = tab.getPosition();

		viewPager.setCurrentItem(tabPosition);
	}

	@Override
	public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {
		//Do nothing for now
	}

	@Override
	public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {
		//Do nothing for now
	}

	@Override
	public void onPageSelected(int position) {
		actionBar.setSelectedNavigationItem(position);
	}

	@Override
	public void onPageScrolled(int arg0, float arg1, int arg2) {
		//Do nothing for now
	}

	@Override
	public void onPageScrollStateChanged(int arg0) {
		//Do nothing for now
	}

	public void groupResponse(ResponseEntity<Object> response) {
		HttpStatus status = response.getStatusCode();

		if(status.equals(ACCEPTED)) {
			String registrationId = response.getBody().toString();

			Log.i(Tag.TAG, selectedGroup.toString());
			new GcmGroupService(this).execute(GCM_GROUP_ADD, selectedGroup.getNotificationKeyName(), registrationId, selectedGroup.getNotificationKey());

			Toast.makeText(getApplicationContext(), USER_ADDED_SUCCESS, LENGTH_SHORT).show();
		} else if(status.equals(NOT_FOUND)) {
			Toast.makeText(getApplicationContext(), USER_NOT_FOUND, LENGTH_SHORT).show();
		} else {
			Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
		}
	}

	@SuppressWarnings("unchecked")
	public void calendarResponse(ResponseEntity<Object> response, String uri) {
		HttpStatus status = response.getStatusCode();
		List<CalendarEntry> calendarEntries = new LinkedList<>();

		if(status.equals(HttpStatus.OK)) {
			if(!uri.contains("delete")) {
				List<Object> temp = (List<Object>) response.getBody();

				for(Object o : temp) {
					CalendarEntry cleanedEntry = ResponseCleaner.cleanCalendarEntry(o);

					calendarEntries.add(cleanedEntry);
				}
			}

			notifiers[0].onResultReceived(calendarEntries, getApplicationContext());
		} else {
			Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
		}
	}

    @SuppressWarnings("unchecked")
	private void messagingResponse(ResponseEntity<Object> response, String uri) {
        if(response != null) {
            HttpStatus status = response.getStatusCode();

            if(status.equals(HttpStatus.OK) && uri.contains("getMessages")) {
                List<Message> messages = new LinkedList<>();

                List<Object> temp = (List<Object>) response.getBody();

                for(Object o : temp) {
                    Message message = ResponseCleaner.cleanMessage(o);

                    messages.add(message);
                }

                notifiers[1].onResultReceived(messages, getApplicationContext());
            }
        }
	}

	@SuppressWarnings("unchecked")
	private void toDoListResponse(ResponseEntity<Object> response, String uri) {
		HttpStatus status = response.getStatusCode();
		List<ToDoListEntry> toDoListEntries = new LinkedList<>();

		if(status.equals(HttpStatus.OK)) {
			if(!uri.contains("delete")) {
				List<Object> temp = (List<Object>) response.getBody();

				for(Object o : temp) {
					ToDoListEntry toDoListEntry = ResponseCleaner.cleanToDoListEntry(o);

					toDoListEntries.add(toDoListEntry);
				}
			}

			notifiers[2].onResultReceived(toDoListEntries, getApplicationContext());
		} else {
			Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
		}
	}
}