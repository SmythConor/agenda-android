package co.agendaapp.group;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.Toast;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import co.agendaapp.R;
import co.agendaapp.helpers.BundleHelper;
import co.agendaapp.model.Group;
import co.agendaapp.model.User;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.dialog.AddCreateDialog;

import static android.widget.Toast.LENGTH_SHORT;
import static co.agendaapp.constants.DialogConstants.REMOVE_MEMBER_BUTTON;
import static co.agendaapp.constants.DialogConstants.REMOVE_MEMBER_HINT;
import static co.agendaapp.constants.DialogConstants.REMOVE_MEMBER_MESSAGE;
import static co.agendaapp.constants.DialogConstants.UPDATE_NAME_BUTTON;
import static co.agendaapp.constants.DialogConstants.UPDATE_NAME_HINT;
import static co.agendaapp.constants.DialogConstants.UPDATE_NAME_MESSAGE;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;

/**
 * Group settings layout class
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-03
 */
public class GroupSettings extends Activity implements OnItemClickListener,
		AddCreateDialog.AddMemberDialogListener, HttpRequestTask.ResultListener {

	private String URI = "";
	private Group selectedGroup;
	private ResponseEntity<Object> response;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_group_settings);

		selectedGroup = BundleHelper.getSelectedGroup(getIntent().getExtras());

		String title = "Settings: " + selectedGroup.getName();
		setTitle(title);

		ListView listView = (ListView) findViewById(R.id.settingsList);
		listView.setOnItemClickListener(this);
	}

	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		Integer selection = position;

		switch(selection) {
			case 0:
				openChangeNameDialog();
				Toast.makeText(getApplicationContext(), "Not implemented", Toast.LENGTH_SHORT).show();
				break;
			case 1:
				Toast.makeText(getApplicationContext(), "Not implemented", LENGTH_SHORT).show();
				break;
			case 2:
				openRemoveMemberDialog();
				Toast.makeText(getApplicationContext(), "Not implemented", Toast.LENGTH_SHORT).show();
				break;
		}
	}

	private void openChangeNameDialog() {
		AddCreateDialog addCreateDialog = new AddCreateDialog();

		addCreateDialog.setHint(UPDATE_NAME_HINT);
		addCreateDialog.setMessage(UPDATE_NAME_MESSAGE);
		addCreateDialog.setPositiveButton(UPDATE_NAME_BUTTON);

		addCreateDialog.show(getFragmentManager(), UPDATE_NAME_MESSAGE);
	}

	private void openRemoveMemberDialog() {
		AddCreateDialog addCreateDialog = new AddCreateDialog();

		addCreateDialog.setHint(REMOVE_MEMBER_HINT);
		addCreateDialog.setMessage(REMOVE_MEMBER_MESSAGE);
		addCreateDialog.setPositiveButton(REMOVE_MEMBER_BUTTON);

		addCreateDialog.show(getFragmentManager(), REMOVE_MEMBER_MESSAGE);
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String value) {
		String dialogIdentifier = dialog.getTag();

		if(dialogIdentifier.equals(UPDATE_NAME_MESSAGE)) {
			changeName(value);
		} else if(dialogIdentifier.equals(REMOVE_MEMBER_MESSAGE)) {
			removeMember(value);
		}
	}

	private void changeName(String name) {
		URI = "/group/updateName/" + selectedGroup.getGroupId();

		selectedGroup.setName(name);

		new HttpRequestTask(URI, HttpMethod.PUT, GroupSettings.this).execute(selectedGroup);
	}

	private void removeMember(String email) {
		URI = "/group/removeMember/" + selectedGroup.getGroupId();

		User user = new User();
		user.setEmail(email);

		new HttpRequestTask(URI, HttpMethod.DELETE, GroupSettings.this).execute(user);
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String uri) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;

		if(response != null) {
			HttpStatus status = response.getStatusCode();

			if(URI.contains("updateName")) {
				if(status.equals(HttpStatus.OK)) {
					Toast.makeText(getApplicationContext(), "User removed successfully", LENGTH_SHORT).show();
				} else if(status.equals(HttpStatus.UNAUTHORIZED)) {
					Toast.makeText(getApplicationContext(), "Only admin can remove members", LENGTH_SHORT).show();
				} else if(status.equals(HttpStatus.NOT_FOUND)) {
					Toast.makeText(getApplicationContext(), "User not in group!", LENGTH_SHORT).show();
				}
			} else {
				if(status.equals(HttpStatus.OK)) {
					selectedGroup = (Group) response.getBody();

					Toast.makeText(getApplicationContext(), "Name changed successfully", LENGTH_SHORT).show();
				} else {
					Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
				}
			}
		} else {
			Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
		}
	}
}