package co.agendaapp.constants;

/**
 * Constants for titles
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-09
 */
public class TitleConstants {
	public static final String GROUP_TITLE = "Group: ";
	public static final String SETTINGS_TITLE = "Settings: ";
}
