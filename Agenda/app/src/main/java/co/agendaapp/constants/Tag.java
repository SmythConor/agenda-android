package co.agendaapp.constants;

/**
 * {Insert Description}
 *
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-02
 */
public class Tag {
    public static final String TAG = "agenda";
}
