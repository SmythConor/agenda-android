package co.agendaapp.constants;

import android.util.Log;

import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

/**
 * Class for service constants
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-01-31
 */
@SuppressWarnings("unused")
public class ServiceConstants {
	public static final String BASE_URL = "http://178.62.98.122:8080/agenda-mt";
	public static final String LOCAL_URL = "http://10.0.2.2:8080/agenda-mt";
	public static final String TEMP_URL = "http://192.168.0.29:8080/agenda-mt";

	public static final String SIGN_UP_URI = "/signUp";
	public static final String SIGN_IN_URI = "/signIn";
	public static final String GROUP_URI = "/group";
	public static final String CALENDAR_URI = "/calendar";
	public static final String TO_DO_LIST_URI = "/toDoList";
    public static final String MESSAGE_URI = "/message";

	public static final String GCM_CREATE_URL = "https://android.googleapis.com/gcm/notification";
	public static final String GCM_AUTH_KEY = "key=AIzaSyD_anE-fYU6aFw-OwrWAmWVP36-3mZ0xSE";
	public static final String GCM_PROJECT_ID = "795145934105";
	public static final String GCM_OPERATION = "operation";
	public static final String GCM_REG_IDS = "registration_ids";
	public static final String GCM_NOTIFICATION_KEY_NAME = "notification_key_name";
	public static final String GCM_NOTIFICATION_KEY = "notification_key";
	public static final String GCM_GROUP_ADD = "add";
	public static final String GCM_GROUP_CREATE = "create";
	public static final String GCM_SEND = "https://gcm-http.googleapis.com/gcm/send";
	public static final String GCM_TO = "to";
	public static final String GCM_DATA = "data";
	public static final String GCM_MESSAGE = "message";

    public static final String ENTRY_CREATED_SUCCESS = "Entry created successfully";
    public static final String ENTRY_UPDATED_SUCCESS = "Entry updated successfully";

    public static final String USER_DOES_NOT_EXIST = "User with that email does not exist";

	public static final String NET_ERR = "Network error";

	private static final List<HttpStatus> statuses = Arrays.asList(
			HttpStatus.OK,
			HttpStatus.CREATED,
			HttpStatus.ACCEPTED,
			HttpStatus.UNAUTHORIZED,
			HttpStatus.UNPROCESSABLE_ENTITY,
			HttpStatus.NOT_FOUND,
			HttpStatus.INTERNAL_SERVER_ERROR
	);

	public static List<HttpStatus> getHttpStatuses() {
		return statuses;
	}

	/**
	 * Uris that should not show progress dialog added here
	 */
	private static final List<String> swipeLayouts = Arrays.asList(
			"getGroups",
			"getEntries"
	);

	public static boolean isSwipeLayout(String uri) {
		for(String swipeLayout : swipeLayouts) {
			if(uri.contains(swipeLayout)) {
				return true;
			}
		}

		return false;
	}
}