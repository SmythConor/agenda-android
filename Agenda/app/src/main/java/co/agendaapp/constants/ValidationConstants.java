package co.agendaapp.constants;

/**
 * Class for validation strings
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-11
 */
public class ValidationConstants {
	public static final String BLANK_NAME = "Name cannot be blank";
	public static final String BLANK_EMAIL = "Email cannot be blank";
	public static final String BLANK_PASSWORD = "Password cannot be blank";

	public static final Integer MAX_LENGTH_NAME = 100;
	public static final String NAME_TOO_LONG = "Name must be below 100 characters";
	public static final Integer MIN_LENGTH_EMAIL = 5;
	public static final Integer MAX_LENGTH_EMAIL = 100;
	public static final String EMAIL_LENGTH_ERR = "Email must be longer than 5 characters and " +
			"shorter than 100 characters";
	public static final Integer MIN_LENGTH_PASSWORD = 5;
	public static final Integer MAX_LENGTH_PASSWORD = 24;
	public static final String PASSWORD_LENGTH_ERR = "Password must be longer than 5 characters " +
			"and shorter than 24 characters";

	public static final String EMAIL_REGEX = ".+@.+\\..+";
	public static final String PASSWORD_MISMATCH = "Password does not match";

	public static final String SIGN_UP_SUCCESS = "Registration successful";
	public static final String DUPLICATE_EMAIL = "User with that email already exists";
	public static final String INVALID_EMAIL = "Email invalid format";

	public static final int NAME_INDEX = 0;
	public static final int EMAIL_INDEX = 1;
	public static final int PASSWORD_INDEX = 2;
	public static final int RE_PASSWORD_INDEX = 3;
	public static final int FIELD_SIZE = 4;

	public static final String STRING_EMPTY = "";

	public static final String LOG_IN_SUCCESS = "Log in successful";
	public static final String LOG_OUT_SUCCESS = "Logged out";
	public static final String LOG_OUT_FAIL = "Error logging out";

	public static final String PASSWORD_INCORRECT = "Password incorrect";
	public static final String NOT_VERIFIED = "Not verified";

	public static final String BLANK_GROUP_NAME = "Group name cannot be blank";
	public static final Integer MAX_LENGTH_GROUP_NAME = 100;
	public static final String GROUP_LENGTH_ERR = "Group name must be must be below 100 characters";

	public static final String USER_ADDED_SUCCESS = "User added successfully";
	public static final String USER_NOT_FOUND = "User not found";

	public static final String GROUP_CREATED_SUCCESS = "Group created successfully";

	public static final String CREATED_SUCCESS = "Entry created successfully";

	public static final String BLANK_TITLE = "Title cannot be blank";
}