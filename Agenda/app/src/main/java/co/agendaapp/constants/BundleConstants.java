package co.agendaapp.constants;

/**
 * Constants for the bundles
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-30
 */
public class BundleConstants {
	public static final String GROUP = "selectedGroup";
	public static final String MESSAGE = "message";

    public static final String CALENDAR_ENTRY = "calendar_entry";
    public static final String TODO_LIST_ENTRY = "todo_list_entry";

    public static final String OP = "operation";
    public static final String CREATE_OP = "Create";
    public static final String EDIT_OP = "Edit";
}