package co.agendaapp.constants;

/**
 * Dialog constants for dialogs
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-09
 */
public class DialogConstants {
	public static final String CANCEL = "Cancel";

	public static final String CREATE_GROUP_HINT = "Name";
	public static final String CREATE_GROUP_MESSAGE = "Create Group";
	public static final String CREATE_GROUP_BUTTON = "Create";

	public static final String ADD_MEMBER_HINT = "email";
	public static final String ADD_MEMBER_MESSAGE = "Add member";
	public static final String ADD_MEMBER_BUTTON = "Add member";

	public static final String UPDATE_NAME_HINT = "name";
	public static final String UPDATE_NAME_MESSAGE = "Update Name";
	public static final String UPDATE_NAME_BUTTON = "Update";

	public static final String REMOVE_MEMBER_HINT = "email";
	public static final String REMOVE_MEMBER_MESSAGE = "Remove Member";
	public static final String REMOVE_MEMBER_BUTTON = "Remove";
}
