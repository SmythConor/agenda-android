package co.agendaapp.cache;

/**
 * Cache file name constant class
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-02
 */
public class CacheFileNames {
	public static final String GROUP_CACHE_NAME = "cache_group";
	public static final String USER_CACHE_NAME = "cache_user";
	public static final String REG_ID_CACHE_NAME = "cache_gcm_reg_token";
}
