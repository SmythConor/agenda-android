package co.agendaapp.cache;

import android.util.Log;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

import co.agendaapp.constants.Tag;

/**
 * Cache Helper class for storing things in the cache
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-02
 */
public class CacheHelper {

	public static void write(File cacheDir, String fileName, Object obj) {
		try {
			File cache = getCache(cacheDir, fileName);

			new ObjectMapper().writeValue(cache, obj);
		} catch(FileNotFoundException e) {
			Log.e(Tag.TAG, "Unable to open cache");
		} catch(IOException e) {
			Log.e(Tag.TAG, e.getMessage());
		}
	}

	/**
	 * Read an object from a cache file
	 * @param cacheDir the get directory
	 * @param fileName the file to read
	 * @param clazz the type of object to read
	 * @return the object read
	 */
	public static Object read(File cacheDir, String fileName, Class<?> clazz) {
		Object obj = new Object();

		try {
			File cache = getCache(cacheDir, fileName);

			InputStream in = new BufferedInputStream(new FileInputStream(cache));

			obj = new ObjectMapper().readValue(in, clazz);

			in.close();
		} catch(FileNotFoundException e) {
			Log.e(Tag.TAG, "Unable to open cache");
		} catch(IOException e) {
			Log.e(Tag.TAG, "Unable to read cache");
		}

		return obj;
	}

	public static File getCache(File cacheDir, String fileName) {
		return new File(cacheDir, fileName);
	}
}