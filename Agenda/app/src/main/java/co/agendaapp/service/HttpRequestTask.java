package co.agendaapp.service;

import android.support.v4.app.Fragment;
import android.util.Log;
import android.app.Activity;
import android.os.AsyncTask;
import android.app.ProgressDialog;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
/*import com.fasterxml.jackson.datatype.joda.JodaMapper;
import com.fasterxml.jackson.datatype.joda.JodaModule;
import com.fasterxml.jackson.datatype.joda.cfg.JacksonJodaDateFormat;*/
import com.kaopiz.kprogresshud.KProgressHUD;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpHeaders;

import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;

import java.io.IOException;
import java.net.ConnectException;
import java.sql.Date;
import java.util.List;

import co.agendaapp.constants.Tag;
import co.agendaapp.utils.DateTimeSerializer;

import static co.agendaapp.constants.ServiceConstants.BASE_URL;
import static co.agendaapp.constants.ServiceConstants.TEMP_URL;
import static co.agendaapp.constants.ServiceConstants.LOCAL_URL;
import static co.agendaapp.constants.ServiceConstants.isSwipeLayout;
import static org.springframework.http.MediaType.APPLICATION_JSON;

/**
 * HttpRequestTask class for handling service requests to the server
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-01-31
 */
public class HttpRequestTask extends AsyncTask<Object, Void, ResponseEntity<Object>> {
	private ResponseEntity<Object> response;

	private String uri;
	private HttpMethod method;

	private KProgressHUD loadingBar;

	private RestTemplate restTemplate;

	private Activity activity;
	private ResultListener resultListener;

	public interface ResultListener {
		public void onResultReceived(Activity activity, Object value, String url);
	}

	public HttpRequestTask(String uri, HttpMethod method, Activity activity) {
		this.uri = uri;
		this.method = method;

		restTemplate = new RestTemplate();

		this.activity = activity;
		resultListener = (ResultListener) this.activity;

		ObjectMapper mapper = new ObjectMapper();
		SimpleModule module = new SimpleModule();

		module.addSerializer(new DateTimeSerializer());
		mapper.registerModule(module);

		MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();

		List<HttpMessageConverter<?>> converters = restTemplate.getMessageConverters();

		for(HttpMessageConverter<?> conv : converters) {
			if(conv instanceof MappingJackson2HttpMessageConverter) {
				converter = (MappingJackson2HttpMessageConverter) conv;
			}
		}

		converter.setObjectMapper(mapper);

		restTemplate.getMessageConverters().add(converter);
		restTemplate.getMessageConverters().add(new StringHttpMessageConverter());
		restTemplate.setErrorHandler(new HttpRequestErrorHandler(this.activity));

		this.loadingBar = KProgressHUD.create(activity);

		loadingBar.setStyle(KProgressHUD.Style.SPIN_INDETERMINATE)
				.setLabel("Please wait")
				.setAnimationSpeed(2)
				.setDimAmount(0.5f);
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();

		if (!isSwipeLayout(uri) && !uri.contains("message")) {
            loadingBar.show();
		}
	}

	@Override
	protected void onPostExecute(ResponseEntity<Object> obj) {
		super.onPostExecute(obj);

		resultListener.onResultReceived(activity, obj, uri);

		if(loadingBar.isShowing()) {

			loadingBar.dismiss();
		}
	}

	@Override
	protected ResponseEntity<Object> doInBackground(Object... obj) {
		String url = BASE_URL + uri;
		Log.i(Tag.TAG, url);

		try {
			if(method.equals(HttpMethod.GET)) {
				return doGet(url);
			} else if(method.equals(HttpMethod.POST) || method.equals(HttpMethod.PUT)) {
				return doPost(url, obj);
			} else if(method.equals(HttpMethod.DELETE)) {
				return doGet(url);
			}
		} catch(Exception e) {
			Log.e(Tag.TAG, e.toString());
		}

		return null;
	}

	private ResponseEntity<Object> doGet(String url) {
		response = restTemplate.exchange(url, method, null, Object.class);

		return response;
	}

	private ResponseEntity<Object> doPost(String url, Object... obj) {
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(APPLICATION_JSON);
		headers.set("Accept", "application/json");

		HttpEntity<Object> entity = new HttpEntity<>(obj[0], headers);

		Log.i(Tag.TAG, entity.toString());

		response = restTemplate.exchange(url, method, entity, Object.class);

		return response;
	}
}