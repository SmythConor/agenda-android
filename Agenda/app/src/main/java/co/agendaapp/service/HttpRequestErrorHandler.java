package co.agendaapp.service;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import org.springframework.http.HttpStatus;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.web.client.ResponseErrorHandler;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Scanner;

import co.agendaapp.constants.ServiceConstants;
import co.agendaapp.constants.Tag;

import static android.widget.Toast.LENGTH_SHORT;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;

/**
 * Error handler to stop app crashing on results
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-17
 */
public class HttpRequestErrorHandler implements ResponseErrorHandler {
	private Context applcationContext;

	public HttpRequestErrorHandler(Context applcationContext) {
		this.applcationContext = applcationContext;
	}

	@Override
	public boolean hasError(ClientHttpResponse response) throws IOException {
		HttpStatus status = response.getStatusCode();

		return isError(status);
	}

	@Override
	public void handleError(ClientHttpResponse response) throws IOException {
		Toast.makeText(applcationContext, NET_ERR, LENGTH_SHORT).show();
	}

	private boolean isError(HttpStatus status) {
		List<HttpStatus> statuses = ServiceConstants.getHttpStatuses();

		return !statuses.contains(status);
	}
}