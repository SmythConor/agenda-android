package co.agendaapp.service;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.kaopiz.kprogresshud.KProgressHUD;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.util.Arrays;
import java.util.Scanner;

import co.agendaapp.constants.ServiceConstants;
import co.agendaapp.constants.Tag;
import co.agendaapp.utils.PropertyGenerator;

import static co.agendaapp.constants.ServiceConstants.GCM_AUTH_KEY;
import static co.agendaapp.constants.ServiceConstants.GCM_CREATE_URL;
import static co.agendaapp.constants.ServiceConstants.GCM_GROUP_ADD;
import static co.agendaapp.constants.ServiceConstants.GCM_NOTIFICATION_KEY;
import static co.agendaapp.constants.ServiceConstants.GCM_OPERATION;
import static co.agendaapp.constants.ServiceConstants.GCM_NOTIFICATION_KEY_NAME;
import static co.agendaapp.constants.ServiceConstants.GCM_PROJECT_ID;
import static co.agendaapp.constants.ServiceConstants.GCM_REG_IDS;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;

/**
 * Get a notification key from gcm for messsaging within group
 * Params will be used for different operations and
 * group specific functions
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-22
 */
public class GcmGroupService extends AsyncTask<String, Void, String> {

	private URL url;
	private HttpURLConnection httpURLConnection;
	private String notificationKey;

	private Activity activity;

	public GcmGroupService(Activity activity) {
		this.activity = activity;
	}

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected String doInBackground(String... params) {
		/* The operation i.e add, create etc */
		String operation = params[0];

		/* The name for the group */
		String notificationKeyName = params[1];

		/* The user creating the group */
		String registrationId = params[2];

		if(params.length == 4) {
			this.notificationKey = params[3];
		}

		try {
			this.url = new URL(GCM_CREATE_URL);
		} catch(MalformedURLException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* open connection */
		try {
			this.httpURLConnection = (HttpURLConnection) url.openConnection();
		} catch(IOException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* Set request method */
		try {
			this.httpURLConnection.setRequestMethod("POST");
		} catch(ProtocolException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* Set up headers */
		this.httpURLConnection.setDoOutput(true);
		this.httpURLConnection.setDoInput(true);
		this.httpURLConnection.setRequestProperty("Content-Type", "application/json");
		this.httpURLConnection.setRequestProperty("Authorization", GCM_AUTH_KEY);
		this.httpURLConnection.setRequestProperty("project_id", GCM_PROJECT_ID);

		/* Connect */
		try {
			this.httpURLConnection.connect();
		} catch(IOException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* Create json body and add fields */
		JSONObject requestBody = new JSONObject();
		try {
			requestBody.put(GCM_OPERATION, operation);
			if(operation.equals(GCM_GROUP_ADD)) {
				requestBody.put(GCM_NOTIFICATION_KEY, this.notificationKey);
			}

			requestBody.put(GCM_NOTIFICATION_KEY_NAME, notificationKeyName);
			requestBody.put(GCM_REG_IDS, new JSONArray(Arrays.asList(new String[]{registrationId})));
			Log.i(Tag.TAG, requestBody.toString());
		} catch(JSONException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* Write the json object to connection */
		try {
			OutputStream os = this.httpURLConnection.getOutputStream();
			os.write(requestBody.toString().getBytes("UTF-8"));
			os.close();
		} catch(IOException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		try {
			InputStream is;

			if(this.httpURLConnection.getResponseCode() != 200) {
				/* if not 200, log error and return error */
				is = this.httpURLConnection.getErrorStream();

				String response = new Scanner(is, "UTF-8").useDelimiter("\\A").next();

				Log.e(Tag.TAG, response);

				is.close();

				return NET_ERR;
			} else {
				/* if is 200, extract key and return */
				is = this.httpURLConnection.getInputStream();

				String response = new Scanner(is, "UTF-8").useDelimiter("\\A").next();

				Log.i(Tag.TAG, response);

				try {
					JSONObject responseBody = new JSONObject(response);

					this.notificationKey = responseBody.getString(GCM_NOTIFICATION_KEY);

					return notificationKey;
				} catch(JSONException e) {
					Log.e(Tag.TAG, e.getMessage());
				}
			}
		} catch(IOException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		return NET_ERR;
	}

	@Override
	protected void onPostExecute(String s) {
		super.onPostExecute(s);
	}
}