package co.agendaapp.utils;

import android.util.Log;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.DeserializationContext;

import java.io.IOException;
import java.util.Date;

import org.joda.time.DateTime;

import co.agendaapp.constants.Tag;

/**
 * Desializer for datetimes
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-26
 */
public class DateTimeDeserializer extends JsonDeserializer<DateTime> {
    @Override
    public Class<?> handledType() {
        return DateTime.class;
    }

    @Override
    public DateTime deserialize(JsonParser p, DeserializationContext ctxt) throws IOException {
        long value = p.getValueAsLong();

        return new DateTime(value);
    }

    /*@Override
    public DateTime deserialize(JsonParser p, DeserializationContext ctxt, DateTime intoValue) throws IOException {
        return intoValue != null ? new DateTime(intoValue.getMillis()) : null;
    }*/
}