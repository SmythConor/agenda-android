package co.agendaapp.utils;

import android.util.Log;

import co.agendaapp.constants.Tag;

/**
 * {Insert Description}
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-08
 */
public class PropertyGenerator {
	public static String generateString(int length) {
		String s = "";

		for(int i = 0; i < length; i++) {
			s += "a";
		}

		return s;
	}
}