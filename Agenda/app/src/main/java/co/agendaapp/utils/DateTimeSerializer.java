package co.agendaapp.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

import org.joda.time.DateTime;

import java.io.IOException;

/**
 * Date time serializer for jackson
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-23
 */
public class DateTimeSerializer extends JsonSerializer<DateTime> {

	@Override
	public Class<DateTime> handledType() {
		return DateTime.class;
	}

	@Override
	public void serialize(DateTime value, JsonGenerator gen, SerializerProvider serializers) throws IOException {
		if(value != null) {
			gen.writeNumber(value.getMillis());
		}
	}
}