package co.agendaapp.utils;

import org.joda.time.DateTime;

import java.util.Comparator;

import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;

/**
 * Compartor for to do list entry list
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-05-16
 */
public class ToDoListComparator implements Comparator<ToDoListEntry> {
	@Override
	public int compare(ToDoListEntry lhs, ToDoListEntry rhs) {
		if(lhs != null && rhs != null) {
			DateTime ldt = lhs.getReminderTimestamp();
			DateTime rdt = rhs.getReminderTimestamp();

            if(ldt == null) {
                ldt = lhs.getInsertTimestamp();
            }

            if(rdt == null) {
                rdt = rhs.getInsertTimestamp();
            }

			Priority l = lhs.getPriority();
			Priority r = rhs.getPriority();

            if (ldt.equals(rdt)) {
                return comparePriorities(l, r);
            } else if (ldt.isBefore(rdt)) {
                return comparePriorities(l, r);
            } else {
                return comparePriorities(l, r);
            }
		}

		return -1;
	}

	private int comparePriorities(Priority lhs, Priority rhs) {
		int compared = lhs.compareTo(rhs);

		if(compared == 0) {
			return 0;
		} else if(compared < 0) {
			return -1;
		} else {
			return 1;
		}
	}
}