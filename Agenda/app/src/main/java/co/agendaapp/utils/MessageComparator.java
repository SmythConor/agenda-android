package co.agendaapp.utils;

import org.joda.time.DateTime;

import java.util.Comparator;

import co.agendaapp.model.Message;

/**
 * Compartor for message list
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @since 2016-05-27
 */
public class MessageComparator implements Comparator<Message> {
    @Override
    public int compare(Message lhs, Message rhs) {
        if(lhs != null && rhs != null) {
            DateTime ldt = lhs.getSentTimestamp();
            DateTime rdt = rhs.getSentTimestamp();

            if(ldt.equals(rdt)) {
                return 0;
            }

            if(ldt.isBefore(rdt)) {
                return -1;
            }

            return 1;
        }

        return 0;
    }
}
