package co.agendaapp.utils;

import org.joda.time.DateTime;

import java.util.Map;

import co.agendaapp.model.Message;
import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.model.Verified;
import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.model.CalendarEntry;

/**
 * response cleaner for cleaning the response from the service
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-30
 */
@SuppressWarnings("unchecked")
public class ResponseCleaner {
	public static User cleanUser(Object o) {
		Map<Object, Object> obj = (Map) o;

		Integer userId = (Integer) obj.get("userId");
		String name = (String) obj.get("name");
		String email = (String) obj.get("email");
		String temp = (String) obj.get("isVerified");

		Verified isVerified = temp.equals(Verified.IS.toString()) ? Verified.IS : Verified.NOT;

		User user = new User();

		user.setUserId(userId);
		user.setName(name);
		user.setEmail(email);
		user.setIsVerified(isVerified);

		return user;
	}

	public static Group cleanGroup(Object object) {
		Map<Object, Object> obj = (Map) object;

		Integer groupId = (Integer) obj.get("groupId");
		Integer groupAdminId = (Integer) obj.get("groupAdminId");
		String name = (String) obj.get("name");
		String notificationKey = (String) obj.get("notificationKey");
		String notificationKeyName = (String) obj.get("notificationKeyName");

		Group group = new Group();

		group.setGroupId(groupId);
		group.setGroupAdminId(groupAdminId);
		group.setName(name);
		group.setNotificationKey(notificationKey);
		group.setNotificationKeyName(notificationKeyName);

		return group;
	}

	public static CalendarEntry cleanCalendarEntry(Object o) {
		Map<Object, Object> obj = (Map) o;

		Integer entryId = (Integer) obj.get("entryId");
		Integer groupId = (Integer) obj.get("groupId");
		Integer userId = (Integer) obj.get("userId");
		String title = (String) obj.get("title");
		String description = (String) obj.get("description");
		Integer recurCount = (Integer) obj.get("recurCount");
		DateTime entryTimestamp = new DateTime(obj.get("entryTimestamp"));
        DateTime reminderTimestamp = getTimestamp(obj, "reminderTimestamp");
        DateTime insertTimestamp = getTimestamp(obj,"insertTimestamp");
        DateTime updateTimestamp = getTimestamp(obj, "updateTimestamp");
		Integer lastUpdateId = (Integer) obj.get("lastUpdateId");
		String updateComment = (String) obj.get("updateComment");

		CalendarEntry calendarEntry = new CalendarEntry();

		calendarEntry.setEntryId(entryId);
		calendarEntry.setGroupId(groupId);
		calendarEntry.setUserId(userId);
		calendarEntry.setTitle(title);
		calendarEntry.setDescription(description);
		calendarEntry.setRecurCount(recurCount);
		calendarEntry.setEntryTimestamp(entryTimestamp);
		calendarEntry.setReminderTimestamp(reminderTimestamp);
		calendarEntry.setInsertTimestamp(insertTimestamp);
		calendarEntry.setUpdateTimestamp(updateTimestamp);
		calendarEntry.setLastUpdateId(lastUpdateId);
		calendarEntry.setUpdateComment(updateComment);

		return calendarEntry;
	}

	public static ToDoListEntry cleanToDoListEntry(Object o) {
		Map<Object, Object> obj = (Map) o;

		Integer entryId = (Integer) obj.get("entryId");
		Integer groupId = (Integer) obj.get("groupId");
		Integer userId = (Integer) obj.get("userId");
		String title = (String) obj.get("title");
		String description = (String) obj.get("description");
		String tempPriority = (String) obj.get("priority");
		DateTime reminderTimestamp = getTimestamp(obj, "reminderTimestamp");
		DateTime insertTimestamp = getTimestamp(obj, "insertTimestamp");
		DateTime updateTimestamp = getTimestamp(obj, "updateTimestamp");
		Integer lastUpdateId = (Integer) obj.get("lastUpdateId");
		String updateComment = (String) obj.get("updateComment");

		Priority priority;

		if(Priority.HIGH.toString().equals(tempPriority)) {
			priority = Priority.HIGH;
		} else if(Priority.MEDIUM.toString().equals(tempPriority)) {
			priority = Priority.MEDIUM;
		} else {
			priority = Priority.LOW;
		}

		ToDoListEntry toDoListEntry = new ToDoListEntry();

		toDoListEntry.setEntryId(entryId);
		toDoListEntry.setGroupId(groupId);
		toDoListEntry.setUserId(userId);
		toDoListEntry.setTitle(title);
		toDoListEntry.setDescription(description);
		toDoListEntry.setPriority(priority);
		toDoListEntry.setReminderTimestamp(reminderTimestamp);
		toDoListEntry.setInsertTimestamp(insertTimestamp);
		toDoListEntry.setUpdateTimestamp(updateTimestamp);
		toDoListEntry.setLastUpdateId(lastUpdateId);
		toDoListEntry.setUpdateComment(updateComment);

		return toDoListEntry;
	}

    public static Message cleanMessage(Object o) {
        Map<Object, Object> obj = (Map) o;

        Integer messageId = (Integer) obj.get("messageId");
        String messageBody = (String) obj.get("messageBody");
        Integer groupId = (Integer) obj.get("groupId");
        Integer senderId = (Integer) obj.get("senderId");
        DateTime sentTimestamp = getTimestamp(obj, "sentTimestamp");
        DateTime deliveredTimestamp = getTimestamp(obj, "deliveredTimestamp");

        Message message = new Message();

        message.setMessageId(messageId);
        message.setMessageBody(messageBody);
        message.setGroupId(groupId);
        message.setSenderId(senderId);
        message.setSentTimestamp(sentTimestamp);
        message.setDeliveredTimestamp(deliveredTimestamp);

        return message;
    }

	private static DateTime getTimestamp(Map<Object, Object> map, String field) {
		Object val = map.get(field);

        return val == null ? null : new DateTime(val);
	}
}
