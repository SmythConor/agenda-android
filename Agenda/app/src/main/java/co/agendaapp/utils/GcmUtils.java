package co.agendaapp.utils;

import java.util.UUID;

/**
 * Some utils for gcm
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-22
 */
public class GcmUtils {
	public static String getNotificationKeyName(String name) {
		UUID randomUUID = UUID.randomUUID();

		return name + "-" + randomUUID.toString();
	}
}
