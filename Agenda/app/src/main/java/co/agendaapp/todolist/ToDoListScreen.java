package co.agendaapp.todolist;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.View;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.text.InputType;
import android.app.AlertDialog;
import android.widget.ListView;
import android.widget.TextView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.widget.AdapterView;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.Toast;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.github.clans.fab.FloatingActionButton;

import org.joda.time.DateTime;
import org.springframework.http.HttpMethod;

import java.util.Collections;
import java.util.List;

import co.agendaapp.R;
import co.agendaapp.constants.Tag;
import co.agendaapp.group.GroupScreen;
import co.agendaapp.model.Group;
import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.adapter.ToDoListEntryAdapter;
import co.agendaapp.utils.DateTimeSerializer;
import co.agendaapp.utils.ToDoListComparator;

import static co.agendaapp.constants.BundleConstants.CREATE_OP;
import static co.agendaapp.constants.BundleConstants.EDIT_OP;
import static co.agendaapp.constants.BundleConstants.OP;
import static co.agendaapp.constants.BundleConstants.TODO_LIST_ENTRY;
import static co.agendaapp.constants.ServiceConstants.TO_DO_LIST_URI;
import static co.agendaapp.helpers.BundleHelper.getSelectedGroup;

/**
 * To Do list screen
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-16
 */
public class ToDoListScreen extends Fragment implements GroupScreen.ResultListener, AdapterView.OnItemLongClickListener {

	private Group selectedGroup;
	private ListView listView;
	private SwipeRefreshLayout swipeRefreshLayout;
	private ToDoListEntry longClickedToDoListEntry;

	@Nullable
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.activity_to_do_list, container, false);
	}

	@Override
	public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		this.selectedGroup = getSelectedGroup(getArguments());

		final FloatingActionButton createEntryButton = (FloatingActionButton) view.findViewById(R.id.createToDoListEntryFab);
		createEntryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                getArguments().putString(OP, CREATE_OP);

				openCreateEditEntry();
			}
		});

		this.listView = (ListView) view.findViewById(R.id.toDoListList);
		this.listView.setLongClickable(true);
		this.listView.setOnItemLongClickListener(this);
		registerForContextMenu(this.listView);

		this.swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.toDoListSwipeLayoutContainer);
		swipeRefreshLayout.setColorSchemeColors(Color.BLUE);
		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getEntries();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		getEntries();
	}

	private void openCreateEditEntry() {
		final Intent createEditEntryScreen = new Intent(getContext(), CreateEditEntryScreen.class);

		createEditEntryScreen.putExtras(getArguments());

		startActivity(createEditEntryScreen);
	}

	private void getEntries() {
		final String URI = TO_DO_LIST_URI + "/getEntries/" + selectedGroup.getGroupId();

		new HttpRequestTask(URI, HttpMethod.GET, getActivity()).execute();
	}

	private void deleteSelectedEntry() {
		final String URI = TO_DO_LIST_URI + "/deleteEntry/" + longClickedToDoListEntry.getEntryId();

		new HttpRequestTask(URI, HttpMethod.DELETE, getActivity()).execute();

		getEntries();
	}

	private void populateList(List<ToDoListEntry> toDoListEntries) {
		if(toDoListEntries.isEmpty()) {
			ToDoListEntry toDoListEntry = new ToDoListEntry();

			toDoListEntry.setTitle("Example Entry");
			toDoListEntry.setDescription("Example Description");
			toDoListEntry.setPriority(Priority.HIGH);
			toDoListEntry.setReminderTimestamp(DateTime.now());

			toDoListEntries.add(toDoListEntry);
		}

		Collections.sort(toDoListEntries, new ToDoListComparator());

		ArrayAdapter<ToDoListEntry> adapter = new ToDoListEntryAdapter(getContext(), toDoListEntries);

		this.listView.setAdapter(adapter);

		adapter.notifyDataSetChanged();
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Object value, Context context) {
		if(value != null) {
			List<ToDoListEntry> toDoListEntries = (List<ToDoListEntry>) value;

			populateList(toDoListEntries);

			this.swipeRefreshLayout.setRefreshing(false);
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if(v.getId() == R.id.toDoListList) {
			MenuInflater inflater = getActivity().getMenuInflater();

			inflater.inflate(R.menu.menu_entries, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.editEntry:
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    SimpleModule module = new SimpleModule();

                    module.addSerializer(new DateTimeSerializer());
                    mapper.registerModule(module);

                    String selectedToDoListEntry = mapper.writeValueAsString(this.longClickedToDoListEntry);

                    getArguments().putString(TODO_LIST_ENTRY, selectedToDoListEntry);

                    getArguments().putString(OP, EDIT_OP);
                } catch(JsonProcessingException e) {
                    Log.e(Tag.TAG, e.getMessage());
                }

                openCreateEditEntry();

				return true;
			case R.id.deleteEntry:
				Dialog deleteDialog = openDeleteEntryDialog();

				deleteDialog.show();
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		longClickedToDoListEntry = (ToDoListEntry) parent.getItemAtPosition(position);

		Log.i(Tag.TAG, longClickedToDoListEntry.toString());

		return false;
	}

	private Dialog openDeleteEntryDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		final TextView input = new TextView(getActivity().getApplicationContext());

		input.setTextColor(Color.BLACK);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setText("Are you sure?");
		builder.setView(input);

		builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deleteSelectedEntry();
			}
		}).setNegativeButton("Cancal", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}
}
