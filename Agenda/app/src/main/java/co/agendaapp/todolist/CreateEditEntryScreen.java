package co.agendaapp.todolist;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;
import android.app.Activity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.DatePicker;
import android.widget.AdapterView;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

import static android.widget.Toast.LENGTH_SHORT;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.util.Calendar;

import co.agendaapp.R;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.cache.CacheFileNames;

import co.agendaapp.constants.Tag;
import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.helpers.CalendarHelper;
import co.agendaapp.service.HttpRequestTask;

import static co.agendaapp.constants.BundleConstants.CREATE_OP;
import static co.agendaapp.constants.BundleConstants.EDIT_OP;
import static co.agendaapp.constants.BundleConstants.OP;
import static co.agendaapp.constants.ServiceConstants.ENTRY_UPDATED_SUCCESS;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;
import static co.agendaapp.helpers.BundleHelper.getSelectedGroup;
import static co.agendaapp.constants.ServiceConstants.TO_DO_LIST_URI;
import static co.agendaapp.constants.ValidationConstants.BLANK_TITLE;
import static co.agendaapp.constants.ValidationConstants.STRING_EMPTY;
import static co.agendaapp.constants.ValidationConstants.CREATED_SUCCESS;
import static co.agendaapp.helpers.BundleHelper.getSelectedToDoListEntry;

/**
 * Class for handling the creation of a to do list entry
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-04
 */
public class CreateEditEntryScreen extends Activity implements AdapterView
		.OnItemSelectedListener, HttpRequestTask.ResultListener {

	private final Calendar today = Calendar.getInstance();
	private final Calendar reminderDate = Calendar.getInstance();

	private Integer reminderHour = 0;
	private Integer reminderMinutes = 0;

	private TextView reminderDateField;
	private TextView reminderTimeField;

	private Group selectedGroup;
	private String selectedPriority = "High";

	private ToDoListEntry selectedToDoListEntry;

    private String operation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_create_entry_to_do_list);

		Bundle extras = getIntent().getExtras();
		selectedGroup = getSelectedGroup(extras);
        this.operation = (String) extras.get(OP);

        if(this.operation != null && operation.equals(EDIT_OP)) {
            this.selectedToDoListEntry = getSelectedToDoListEntry(extras);

            String title = EDIT_OP + " Entry";

            ((TextView) findViewById(R.id.createEntryTitle)).setText(title);
            assignValues();
        } else {
            this.selectedToDoListEntry = null;
            this.operation = CREATE_OP;

            String title = CREATE_OP + " Entry";

            ((TextView) findViewById(R.id.createEntryTitle)).setText(title);
        }

		final Spinner spinner = (Spinner) findViewById(R.id.prioritySpinner);
		spinner.setOnItemSelectedListener(this);

		initialiseListeners();

		final Button createEntryButton = (Button) findViewById(R.id.createEntryButton);
		createEntryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateFields()) {
                    createEditEntry();
                }
            }
        });
	}

	private void assignValues() {
        String title = this.selectedToDoListEntry.getTitle();
        String description = this.selectedToDoListEntry.getDescription();
        Priority priority = this.selectedToDoListEntry.getPriority();

		((EditText) findViewById(R.id.titleField)).setText(title);

        if(description != null) {
            ((EditText) findViewById(R.id.descriptionField)).setText(description);
        }

		((Spinner) findViewById(R.id.prioritySpinner)).setSelection(getPos(priority));

        DateTime reminderTimestamp = this.selectedToDoListEntry.getReminderTimestamp();

        if(reminderTimestamp != null) {
            Calendar temp = Calendar.getInstance();
            temp.setTimeInMillis(reminderTimestamp.getMillis());

            String date = CalendarHelper.formatDate(temp);
            ((TextView) findViewById(R.id.reminderDateField)).setText(date);

            Integer hour = reminderTimestamp.getHourOfDay();
            Integer mins = reminderTimestamp.getMinuteOfHour();

            String time = CalendarHelper.formatTime(hour, mins);

            ((TextView) findViewById(R.id.reminderTimeField)).setText(time);
        }
	}

	private int getPos(Priority s) {
		if(Priority.HIGH.toString().equals(s.toString())) {
			return 0;
		} else if(Priority.MEDIUM.toString().equals(s.toString())) {
			return 1;
		} else {
			return 2;
		}
	}

	private void createEditEntry() {
		ToDoListEntry toDoListEntry = new ToDoListEntry();

		String title = getEditTextField(R.id.titleField);
		String description = getEditTextField(R.id.descriptionField);
        User user = (User) CacheHelper.read(getCacheDir(), CacheFileNames.USER_CACHE_NAME, User.class);

		toDoListEntry.setGroupId(selectedGroup.getGroupId());
		toDoListEntry.setUserId(user.getUserId());
		toDoListEntry.setTitle(title);
		toDoListEntry.setDescription(description);

		Priority selPriority = Priority.HIGH;

		if(Priority.HIGH.toString().equals(selectedPriority)) {
			selPriority = Priority.HIGH;
		} else if(Priority.MEDIUM.toString().equals(selectedPriority)) {
			selPriority = Priority.MEDIUM;
		} else if(Priority.LOW.toString().equals(selectedPriority)) {
			selPriority = Priority.LOW;
		}

		toDoListEntry.setPriority(selPriority);

		String tempDateReminder = getTextViewField(R.id.reminderDateField);
		String tempTimeReminder = getTextViewField(R.id.reminderTimeField);

		DateTime reminderDateTime = null;

		if(!tempDateReminder.equals("") || !tempDateReminder.equals("")) {
			reminderDateTime = CalendarHelper.formatDateTime(tempTimeReminder, tempDateReminder);
		}

		toDoListEntry.setReminderTimestamp(reminderDateTime);

        if(this.selectedToDoListEntry != null) {
            toDoListEntry.setEntryId(this.selectedToDoListEntry.getEntryId());
            toDoListEntry.setInsertTimestamp(this.selectedToDoListEntry.getInsertTimestamp());
            toDoListEntry.setUpdateTimestamp(this.selectedToDoListEntry.getUpdateTimestamp());
        }

        final String URI = TO_DO_LIST_URI + (operation.equals(CREATE_OP) ? "/createEntry" : "/updateEntry");
        final HttpMethod method = operation.equals(CREATE_OP) ? HttpMethod.POST : HttpMethod.PUT;

		new HttpRequestTask(URI, method, CreateEditEntryScreen.this).execute(toDoListEntry);
	}

	private boolean validateFields() {
		String title = getEditTextField(R.id.titleField);

		if(title.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_TITLE, Toast.LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	private void initialiseListeners() {
		reminderDateField = (TextView) findViewById(R.id.reminderDateField);
		reminderTimeField = (TextView) findViewById(R.id.reminderTimeField);

		reminderDateField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new DatePickerDialog(CreateEditEntryScreen.this, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
						reminderDate.set(year, monthOfYear, dayOfMonth);

						String date = CalendarHelper.formatDate(reminderDate);

						reminderDateField.setText(date);
					}
				}, today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH)).show();
			}
		});

		reminderTimeField.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				new TimePickerDialog(CreateEditEntryScreen.this, new TimePickerDialog.OnTimeSetListener() {
					@Override
					public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
						reminderHour = hourOfDay;
						reminderMinutes = minute;

						String time = CalendarHelper.formatTime(reminderHour, reminderMinutes);

						reminderTimeField.setText(time);
					}
				}, Calendar.HOUR_OF_DAY, Calendar.MINUTE, true).show();
			}
		});
	}

	private String getTextViewField(int resId) {
		return ((TextView) findViewById(resId)).getText().toString().trim();
	}

	private String getEditTextField(int resId) {
		return ((EditText) findViewById(resId)).getText().toString().trim();
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		selectedPriority = ((String) parent.getItemAtPosition(position)).toUpperCase();
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		//do nothing
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String url) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;
		HttpStatus status = response.getStatusCode();

		if(status.equals(HttpStatus.CREATED) || status.equals(HttpStatus.OK)) {
            final String MESSAGE = operation.equals(CREATE_OP) ? CREATED_SUCCESS : ENTRY_UPDATED_SUCCESS;

			Toast.makeText(getApplicationContext(), MESSAGE, LENGTH_SHORT).show();

			finish();
		} else {
            Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
        }
	}
}