package co.agendaapp.encrypt;

import static java.nio.charset.StandardCharsets.UTF_8;
import java.math.BigInteger;
import java.util.Random;

public class EncryptionUtils {
	public static final String HASH_TYPE = "SHA-256";
	
	public static byte[] getSalt() {
        byte[] salt = new byte[16];

        new Random().nextBytes(salt);

        return salt;
    }
	
	public static byte[] concatArray(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];

        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }

    public static String bytesToHex(byte[] data) {
        return (new BigInteger(data)).toString(16);
    }

    @SuppressWarnings("unused")
    public static byte[] hexToBytes(String data) {
        return (new BigInteger(data, 16)).toByteArray();
    }

    public static byte[] stringToBytes(String str) {
        return str.getBytes(UTF_8);
    }
}