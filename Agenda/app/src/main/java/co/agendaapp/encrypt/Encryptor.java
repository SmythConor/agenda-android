package co.agendaapp.encrypt;

import android.util.Log;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import co.agendaapp.constants.Tag;

public class Encryptor {

    private static MessageDigest messageDigest;

	/**
	 * Hash the password with the salt and return as string
	 * @param password as byte array
	 * @param salt as byte array
	 * @return hashed password as string
	 */
    public static String hashPassword(byte[] password, byte[] salt) {
        byte[] dataToHash = EncryptionUtils.concatArray(password, salt);

        try {
            messageDigest = MessageDigest.getInstance(EncryptionUtils.HASH_TYPE);
        } catch (NoSuchAlgorithmException e) {
            Log.e(Tag.TAG, "Error getting digest type");
        }

        messageDigest.update(dataToHash);

        byte[] digest = messageDigest.digest();

        return EncryptionUtils.bytesToHex(digest);
    }
}