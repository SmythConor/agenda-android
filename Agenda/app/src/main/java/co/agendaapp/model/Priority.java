package co.agendaapp.model;

public enum Priority {
	HIGH, MEDIUM, LOW;

	@Override
	public String toString() {
		switch(this) {
			case HIGH:
				return "HIGH";
			case MEDIUM:
				return "MEDIUM";
			default:
				return "LOW";
		}
	}
}
