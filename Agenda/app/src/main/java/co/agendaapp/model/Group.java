package co.agendaapp.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class Group implements Serializable {
	private static final long serialVersionUID = 2381740530165041911L;
	
	private int groupId;
	private int groupAdminId;
	private String name;
	private String notificationKey;
	private String notificationKeyName;
	
	public Group() {}
	
	/**
	 * @param groupId
	 * @param groupAdminId
	 * @param name
	 */
	public Group(int groupId, int groupAdminId, String name, String notificationKey, String notificationKeyName) {
		this.groupId = groupId;
		this.groupAdminId = groupAdminId;
		this.name = name;
		this.notificationKey = notificationKey;
		this.notificationKeyName = notificationKeyName;
	}

	public String getNotificationKeyName() {
		return notificationKeyName;
	}

	public void setNotificationKeyName(String notificationKeyName) {
		this.notificationKeyName = notificationKeyName;
	}

	public int getGroupId() {
		return groupId;
	}

	public void setGroupId(int groupId) {
		this.groupId = groupId;
	}

	public int getGroupAdminId() {
		return groupAdminId;
	}

	public void setGroupAdminId(int groupAdminId) {
		this.groupAdminId = groupAdminId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNotificationKey() {
		return notificationKey;
	}

	public void setNotificationKey(String notificationKey) {
		this.notificationKey = notificationKey;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder("Group{");
		sb.append("groupId=").append(groupId);
		sb.append(", groupAdminId=").append(groupAdminId);
		sb.append(", name='").append(name).append('\'');
		sb.append(", notificationKey='").append(notificationKey).append('\'');
		sb.append(", notificationKeyName='").append(notificationKeyName).append('\'');
		sb.append('}');
		return sb.toString();
	}
}