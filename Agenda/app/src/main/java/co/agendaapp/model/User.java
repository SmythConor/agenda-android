package co.agendaapp.model;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

/**
 * User class for holding user information
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-05
 */
@JsonInclude(Include.NON_NULL)
public class User implements Serializable {
	private static final long serialVersionUID = -3126462353550003480L;
	
	private int userId;
	private String name;
	private String email;
	private String password;
	private String salt;
	private Verified isVerified;
	private String registrationId;
	
	public User() {}
	
	public User(String email, String password) {
		this.email = email;
		this.password = password;
	}
	
	public User(int userId, String name, String email, String password, String salt, Verified isVerified, String registrationId) {
		this.userId = userId;
		this.name = name;
		this.email = email;
		this.password = password;
		this.salt = salt;
		this.isVerified = isVerified;
		this.registrationId = registrationId;
	}

	/**
	 * @return the userId
	 */
	public int getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(int userId) {
		this.userId = userId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the salt
	 */
	public String getSalt() {
		return salt;
	}

	/**
	 * @param salt the salt to set
	 */
	public void setSalt(String salt) {
		this.salt = salt;
	}

	/**
	 * @return the isVerified
	 */
	public Verified getIsVerified() {
		return isVerified;
	}

	/**
	 * @param isVerified the isVerified to set
	 */
	public void setIsVerified(Verified isVerified) {
		this.isVerified = isVerified;
	}
	
	public String getRegistrationId() {
		return registrationId;
	}
	
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("User [userId=");
		builder.append(userId);
		builder.append(", ");
		if(name != null) {
			builder.append("name=");
			builder.append(name);
			builder.append(", ");
		}
		if(email != null) {
			builder.append("email=");
			builder.append(email);
			builder.append(", ");
		}
		if(password != null) {
			builder.append("password=");
			builder.append(password);
			builder.append(", ");
		}
		if(salt != null) {
			builder.append("salt=");
			builder.append(salt);
			builder.append(", ");
		}
		if(isVerified != null) {
			builder.append("isVerified=");
			builder.append(isVerified);
		}
		if(registrationId != null) {
			builder.append("registrationId=");
			builder.append(registrationId);
			builder.append(", ");
		}
		builder.append("]");
		return builder.toString();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		result = prime * result
		        + ((isVerified == null) ? 0 : isVerified.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result
		        + ((password == null) ? 0 : password.hashCode());
		result = prime * result + ((salt == null) ? 0 : salt.hashCode());
		result = prime * result + userId;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if(this == obj)
			return true;
		if(obj == null)
			return false;
		if(getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if(email == null) {
			if(other.email != null)
				return false;
		} else if(!email.equals(other.email))
			return false;
		if(isVerified != other.isVerified)
			return false;
		if(name == null) {
			if(other.name != null)
				return false;
		} else if(!name.equals(other.name))
			return false;
		if(password == null) {
			if(other.password != null)
				return false;
		} else if(!password.equals(other.password))
			return false;
		if(salt == null) {
			if(other.salt != null)
				return false;
		} else if(!salt.equals(other.salt))
			return false;
		if(userId != other.userId)
			return false;
		return true;
	}
}