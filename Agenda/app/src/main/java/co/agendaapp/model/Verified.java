package co.agendaapp.model;

/**
 * enum for verification options
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-05
 */
public enum Verified {
    IS, NOT;

    @Override
    public String toString() {
        switch(this) {
            case IS:
                return "IS";
            default:
                return "NOT";
        }
    }
}