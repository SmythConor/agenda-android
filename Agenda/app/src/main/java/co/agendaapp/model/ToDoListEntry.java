package co.agendaapp.model;

import java.io.Serializable;
import java.util.Comparator;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

@JsonInclude(Include.NON_NULL)
public class ToDoListEntry implements Serializable {
	private static final long serialVersionUID = -5600369577287407357L;
	
	private Integer entryId;
	private Integer groupId;
	private Integer userId;
	private String title;
	private String description;
	private Priority priority;
	private DateTime reminderTimestamp;
	private DateTime insertTimestamp;
	private DateTime updateTimestamp;
	private Integer lastUpdateId;
	private String updateComment;

	/**
	 * @return the entryId
	 */
	public Integer getEntryId() {
		return entryId;
	}
	/**
	 * @param entryId the entryId to set
	 */
	public void setEntryId(Integer entryId) {
		this.entryId = entryId;
	}
	/**
	 * @return the groupId
	 */
	public Integer getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Integer groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the userId
	 */
	public Integer getUserId() {
		return userId;
	}
	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Integer userId) {
		this.userId = userId;
	}
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}
	/**
	 * @return the priority
	 */
	public Priority getPriority() {
		return priority;
	}
	/**
	 * @param priority the priority to set
	 */
	public void setPriority(Priority priority) {
		this.priority = priority;
	}
	/**
	 * @return the reminderTimestamp
	 */
	public DateTime getReminderTimestamp() {
		return reminderTimestamp;
	}
	/**
	 * @param reminderTimestamp the reminderTimestamp to set
	 */
	public void setReminderTimestamp(DateTime reminderTimestamp) {
		this.reminderTimestamp = reminderTimestamp;
	}
	/**
	 * @return the insertTimestamp
	 */
	public DateTime getInsertTimestamp() {
		return insertTimestamp;
	}
	/**
	 * @param insertTimestamp the insertTimestamp to set
	 */
	public void setInsertTimestamp(DateTime insertTimestamp) {
		this.insertTimestamp = insertTimestamp;
	}
	/**
	 * @return the updateTimestamp
	 */
	public DateTime getUpdateTimestamp() {
		return updateTimestamp;
	}
	/**
	 * @param updateTimestamp the updateTimestamp to set
	 */
	public void setUpdateTimestamp(DateTime updateTimestamp) {
		this.updateTimestamp = updateTimestamp;
	}
	/**
	 * @return the lastUpdateId
	 */
	public Integer getLastUpdateId() {
		return lastUpdateId;
	}
	/**
	 * @param lastUpdateId the lastUpdateId to set
	 */
	public void setLastUpdateId(Integer lastUpdateId) {
		this.lastUpdateId = lastUpdateId;
	}
	/**
	 * @return the updateComment
	 */
	public String getUpdateComment() {
		return updateComment;
	}
	/**
	 * @param updateComment the updateComment to set
	 */
	public void setUpdateComment(String updateComment) {
		this.updateComment = updateComment;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ToDoListEntry [");
		if(entryId != null) {
			builder.append("entryId=");
			builder.append(entryId);
			builder.append(", ");
		}
		if(groupId != null) {
			builder.append("groupId=");
			builder.append(groupId);
			builder.append(", ");
		}
		if(userId != null) {
			builder.append("userId=");
			builder.append(userId);
			builder.append(", ");
		}
		if(title != null) {
			builder.append("title=");
			builder.append(title);
			builder.append(", ");
		}
		if(description != null) {
			builder.append("description=");
			builder.append(description);
			builder.append(", ");
		}
		if(priority != null) {
			builder.append("priority=");
			builder.append(priority);
			builder.append(", ");
		}
		if(reminderTimestamp != null) {
			builder.append("reminderTimestamp=");
			builder.append(reminderTimestamp);
			builder.append(", ");
		}
		if(insertTimestamp != null) {
			builder.append("insertTimestamp=");
			builder.append(insertTimestamp);
			builder.append(", ");
		}
		if(updateTimestamp != null) {
			builder.append("updateTimestamp=");
			builder.append(updateTimestamp);
			builder.append(", ");
		}
		if(lastUpdateId != null) {
			builder.append("lastUpdateId=");
			builder.append(lastUpdateId);
			builder.append(", ");
		}
		if(updateComment != null) {
			builder.append("updateComment=");
			builder.append(updateComment);
		}
		builder.append("]");
		return builder.toString();
	}
}