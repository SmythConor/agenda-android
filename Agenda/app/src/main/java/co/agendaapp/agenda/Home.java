package co.agendaapp.agenda;

import android.app.Activity;

import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.os.Bundle;
import android.widget.Toast;
import android.view.MenuItem;
import android.graphics.Color;
import android.content.Intent;
import android.widget.ListView;
import android.view.MenuInflater;
import android.widget.AdapterView;
import android.app.DialogFragment;
import android.widget.ArrayAdapter;
import android.support.v4.widget.SwipeRefreshLayout;
import android.widget.AdapterView.OnItemClickListener;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.core.JsonProcessingException;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.io.File;
import java.util.List;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.concurrent.ExecutionException;

import co.agendaapp.R;
import co.agendaapp.model.User;
import co.agendaapp.model.Group;
import co.agendaapp.constants.Tag;
import co.agendaapp.utils.GcmUtils;
import co.agendaapp.group.GroupScreen;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.registration.SignIn;
import co.agendaapp.cache.CacheFileNames;
import co.agendaapp.utils.ResponseCleaner;
import co.agendaapp.dialog.AddCreateDialog;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.service.GcmGroupService;
import co.agendaapp.adapter.GroupListAdapter;
import co.agendaapp.constants.BundleConstants;
import co.agendaapp.constants.ValidationConstants;
import co.agendaapp.messaging.RegistrationIntentService;

import static android.widget.Toast.LENGTH_SHORT;
import static co.agendaapp.constants.ServiceConstants.GCM_GROUP_CREATE;
import static org.springframework.http.HttpMethod.GET;
import static org.springframework.http.HttpMethod.POST;
import static co.agendaapp.constants.ServiceConstants.NET_ERR;
import static co.agendaapp.constants.ServiceConstants.GROUP_URI;
import static co.agendaapp.constants.ValidationConstants.LOG_OUT_FAIL;
import static co.agendaapp.constants.ValidationConstants.STRING_EMPTY;
import static co.agendaapp.constants.DialogConstants.CREATE_GROUP_HINT;
import static co.agendaapp.constants.DialogConstants.CREATE_GROUP_BUTTON;
import static co.agendaapp.constants.ValidationConstants.LOG_OUT_SUCCESS;
import static co.agendaapp.constants.DialogConstants.CREATE_GROUP_MESSAGE;
import static co.agendaapp.constants.ValidationConstants.BLANK_GROUP_NAME;
import static co.agendaapp.constants.ValidationConstants.GROUP_LENGTH_ERR;
import static co.agendaapp.constants.ValidationConstants.MAX_LENGTH_GROUP_NAME;

/**
 * Home Screen class
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-02-25
 */
public class Home extends Activity implements OnItemClickListener, AddCreateDialog
		.AddMemberDialogListener, HttpRequestTask.ResultListener {
	private SwipeRefreshLayout swipeLayout;
	private List<Group> groups = new ArrayList<>();

	private Integer selectedUserId;

	private String URI = STRING_EMPTY;

	private String notificationKey;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_home);
	}

	@Override
	protected void onStart() {
		super.onStart();

		final Intent registrationIntent = new Intent(this, RegistrationIntentService.class);
		startService(registrationIntent);

		/* Check is the user signed in */
		if(!isSignedIn()) {
			final Intent logInScreen = new Intent(getApplicationContext(), SignIn.class);

			startActivity(logInScreen);
		} else {
			onLoad();
		}
	}

	/**
	 * Get the groups for this user and populate the view
	 */
	private void getGroups() {
		this.URI = GROUP_URI + "/getGroups/" + selectedUserId;

		this.groups = new LinkedList<>();

		new HttpRequestTask(this.URI, GET, Home.this).execute();
	}

	/**
	 * Create the group with the given name
	 * @param name of the group entered
	 */
	public void createGroup(String name) {
		/* Create the uri endpoint */
		this.URI = GROUP_URI + "/createGroup";

		String registrationId = (String) CacheHelper.read(getCacheDir(),CacheFileNames.REG_ID_CACHE_NAME, String.class);
		String notificationKeyName = GcmUtils.getNotificationKeyName(name);

		try {
			this.notificationKey = new GcmGroupService(Home.this).execute(GCM_GROUP_CREATE, notificationKeyName, registrationId).get();
		} catch(ExecutionException | InterruptedException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		/* Create the group to send to service */
		Group group = new Group();

		group.setName(name);
		group.setGroupAdminId(selectedUserId);
		group.setNotificationKeyName(notificationKeyName);
		group.setNotificationKey(this.notificationKey);

		new HttpRequestTask(this.URI, POST, Home.this).execute(group);
	}

	/**
	 * Open selected group
	 */
	private void openGroup(Group selectedGroup) {
				/* Open the group screen */
		final Intent groupScreenIntent = new Intent(getApplicationContext(), GroupScreen.class);

		try {
			String selectedGrp = new ObjectMapper().writeValueAsString(selectedGroup);

			groupScreenIntent.putExtra(BundleConstants.GROUP, selectedGrp);
		} catch(JsonProcessingException e) {
			Log.e(Tag.TAG, e.getMessage());
		}

		startActivity(groupScreenIntent);
	}

	/**
	 * Populate the list with the items supplied
	 * @param groupListItems items to put in the list
	 */
	private void populateList(List<Group> groupListItems) {
		if(groupListItems.size() == 0) {
			Group group = new Group();
			group.setGroupId(0);
			group.setGroupAdminId(0);
			group.setName("No groups");

			groupListItems.add(group);
		}

		for(Group g : groupListItems) {
			Log.i(Tag.TAG, g.toString());
		}

		ArrayAdapter<Group> adapter = new GroupListAdapter(this, groupListItems);

		ListView listView = (ListView) findViewById(R.id.groupList);

		listView.setAdapter(adapter);

		adapter.notifyDataSetChanged();
	}

    /* ListView listener */

	/**
	 * On item click for the list view
	 * @param adapter the list view adapter
	 * @param view the list view
	 * @param position in the list selected
	 * @param id of the item selected
	 */
	@Override
	public void onItemClick(AdapterView<?> adapter, View view, int position, long id) {
		Group selectedGroup = (Group) adapter.getItemAtPosition(position);

		if(selectedGroup.getGroupAdminId() != 0) {
			openGroup(selectedGroup);
		}
	}

	/**
	 * Get the groups for this user and initialise refresh listener
	 */
	private void onLoad() {
		selectedUserId = getUserId();

        /* Get the groups */
		getGroups();

        /* Set listener on groupList */
		ListView groupListView = (ListView) findViewById(R.id.groupList);

		groupListView.setOnItemClickListener(this);

		this.swipeLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_container);
		this.swipeLayout.setColorSchemeColors(Color.BLUE);
		this.swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getGroups();

				swipeLayout.setRefreshing(true);
			}
		});
	}

    /* Dialog stuff */

	/**
	 * open the create group dialog
	 */
	private void openCreateGroupScreen() {
		AddCreateDialog addCreateDialog = new AddCreateDialog();

		addCreateDialog.setHint(CREATE_GROUP_HINT);
		addCreateDialog.setMessage(CREATE_GROUP_MESSAGE);
		addCreateDialog.setPositiveButton(CREATE_GROUP_BUTTON);

		addCreateDialog.show(getFragmentManager(), CREATE_GROUP_MESSAGE);
	}

	@Override
	public void onDialogPositiveClick(DialogFragment dialog, String name) {
		if(validateField(name)) {
			createGroup(name);
		}
	}

	private boolean validateField(String name) {
		Integer nameLength = name.length();

		if(name.equals(STRING_EMPTY)) {
			Toast.makeText(getApplicationContext(), BLANK_GROUP_NAME, LENGTH_SHORT).show();

			return false;
		}

		if(nameLength > MAX_LENGTH_GROUP_NAME) {
			Toast.makeText(getApplicationContext(), GROUP_LENGTH_ERR, LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

    /* User information functions */

	/**
	 * Check is the user signed in
	 * @return true if the cache file exists
	 */
	private boolean isSignedIn() {
		return new File(getCacheDir(), CacheFileNames.USER_CACHE_NAME).isFile();
	}

	/**
	 * Get the user id from the current user
	 * @return user id as integer
	 */
	private Integer getUserId() {
		User user = (User) CacheHelper.read(getCacheDir(), CacheFileNames.USER_CACHE_NAME, User.class);

		return user.getUserId();
	}

	/**
	 * Log a user out
	 */
	private void logOut() {
		if(new File(getCacheDir(), CacheFileNames.USER_CACHE_NAME).delete()) {
			Toast.makeText(getApplicationContext(), LOG_OUT_SUCCESS, LENGTH_SHORT).show();

			this.recreate();
		} else {
			Toast.makeText(getApplicationContext(), LOG_OUT_FAIL, LENGTH_SHORT).show();
		}
	}

    /* Menu functions */

	/**
	 * Create the menu
	 * @param menu the menu to create
	 * @return true
	 */
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();

		inflater.inflate(R.menu.menu_home, menu);

		return true;
	}

	/**
	 * Handle click on menu item
	 * @param item selected item
	 * @return true
	 */
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.menu_create_group:
				openCreateGroupScreen();
				return true;
			case R.id.menu_log_out:
				logOut();
			default:
				return super.onOptionsItemSelected(item);
		}
	}

	/**
	 * Result handler for request
	 * @param activity this activity
	 * @param value the value returned from the request
	 */
	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String uri) {
		ResponseEntity<Object> response = (ResponseEntity<Object>) value;
		swipeLayout.setRefreshing(false);

		if(response != null) {
			HttpStatus status = response.getStatusCode();

			if(status.equals(HttpStatus.OK)) {
				List<Object> grps = (List<Object>) response.getBody();

				for(Object grp : grps) {
					Group g = ResponseCleaner.cleanGroup(grp);

					groups.add(g);
				}

				populateList(groups);
			} else if(status.equals(HttpStatus.CREATED)) {
				Toast.makeText(getApplicationContext(), ValidationConstants.GROUP_CREATED_SUCCESS,
						LENGTH_SHORT).show();

				getGroups();
			} else {
				Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
			}
		} else {
			Toast.makeText(getApplicationContext(), NET_ERR, Toast.LENGTH_SHORT).show();
		}
	}
}