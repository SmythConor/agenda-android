package co.agendaapp.helpers;

import org.joda.time.DateTime;

import java.util.Locale;
import java.util.Calendar;

/**
 * Helper class for calendar
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-01
 */
public class CalendarHelper {

    public static String formatTime(Integer hour, Integer mins) {
        return hour + ":" + mins;
    }

    public static String formatDate(Calendar calendar) {
        Integer year = calendar.get(Calendar.YEAR);
        String month = calendar.getDisplayName(Calendar.MONTH, Calendar.LONG, Locale.ENGLISH);
        Integer day = calendar.get(Calendar.DAY_OF_MONTH);

        return year + "-" + month + "-" + day;
    }

    public static DateTime formatDateTime(String time, String date) {
        String[] times = time.split(":");

        int hours = Integer.parseInt(times[0]);
        int mins = Integer.parseInt(times[1]);

        String[] dates = date.split("-");
        int year = Integer.parseInt(dates[0]);
        int month = stringToMonth(dates[1]);
        int day = Integer.parseInt(dates[2]);

        return new DateTime(year, month, day, hours, mins);
    }

    /**
     * Must add one here as java.util.Calendar is 0-11 for months but Joda is 1-12
     * @param month the month as a string
     * @return the corresponding month as integer 1-12
     */
    public static int stringToMonth(String month) {
        return stringToMonthExt(month) + 1;
    }

    /**
     * Get the month as an integer from string in indexed 0-11
     * @param month the name ofthe month
     * @return the month as an Integer 0-11
     */
    private static int stringToMonthExt(String month) {
        switch(month) {
            case "January":
                return Calendar.JANUARY;
            case "February":
                return Calendar.FEBRUARY;
            case "March":
                return Calendar.MARCH;
            case "April":
                return Calendar.APRIL;
            case "May":
                return Calendar.MAY;
            case "June":
                return Calendar.JUNE;
            case "July":
                return Calendar.JULY;
            case "August":
                return Calendar.AUGUST;
            case "September":
                return Calendar.SEPTEMBER;
            case "October":
                return Calendar.OCTOBER;
            case "November":
                return Calendar.NOVEMBER;
            case "December":
                return Calendar.DECEMBER;
            default:
                return 0;
        }
    }
}