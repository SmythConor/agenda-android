package co.agendaapp.helpers;

import android.net.sip.SipRegistrationListener;
import android.os.Bundle;
import android.util.Log;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationConfig;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.module.SimpleModule;

import org.joda.time.DateTime;

import java.io.IOException;

import co.agendaapp.constants.BundleConstants;
import co.agendaapp.constants.Tag;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.model.Group;
import co.agendaapp.model.ToDoListEntry;
import co.agendaapp.utils.DateTimeDeserializer;

/**
 * Helper class to get selected group
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-09
 */
public class BundleHelper {
	public static Group getSelectedGroup(Bundle extras) {
		String value = (String) extras.get(BundleConstants.GROUP);

		try {
			return new ObjectMapper().readValue(value, Group.class);
		} catch(IOException e) {
			Log.e(Tag.TAG, "Error reading selected group");
            Log.e(Tag.TAG, e.getMessage());
		}

		return null;
	}

    public static CalendarEntry getSelectedCalendarEntry(Bundle extras) {
        String value = (String) extras.get(BundleConstants.CALENDAR_ENTRY);

        try {
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();

            module.addDeserializer(DateTime.class, new DateTimeDeserializer());
            mapper.registerModule(module);

            return mapper.readValue(value, CalendarEntry.class);
        } catch(IOException e) {
            Log.e(Tag.TAG, "Error reading selected calendar entry");
            Log.e(Tag.TAG, e.getMessage());
        }

        return null;
    }

    public static ToDoListEntry getSelectedToDoListEntry(Bundle extras) {
        String value = (String) extras.get(BundleConstants.TODO_LIST_ENTRY);

        try {
            ObjectMapper mapper = new ObjectMapper();
            SimpleModule module = new SimpleModule();

            module.addDeserializer(DateTime.class, new DateTimeDeserializer());
            mapper.registerModule(module);

            return mapper.readValue(value, ToDoListEntry.class);
        } catch(IOException e) {
            Log.e(Tag.TAG, "Error reading selected calendar entry");
            Log.e(Tag.TAG, e.getMessage());
        }

        return null;
    }
}