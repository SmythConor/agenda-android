package co.agendaapp.calendar;

import android.util.Log;
import android.os.Bundle;
import android.view.View;
import android.app.Dialog;
import android.view.MenuItem;
import android.content.Intent;
import android.graphics.Color;
import android.text.InputType;
import android.view.ViewGroup;
import android.content.Context;
import android.app.AlertDialog;
import android.widget.ListView;
import android.widget.TextView;
import android.view.ContextMenu;
import android.view.MenuInflater;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.view.LayoutInflater;
import android.content.DialogInterface;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.github.clans.fab.FloatingActionButton;
import com.marcohc.robotocalendar.RobotoCalendarView;

import org.joda.time.DateTime;
import org.springframework.http.HttpMethod;

import java.util.Date;
import java.util.List;
import java.util.Calendar;
import java.util.LinkedList;

import co.agendaapp.constants.Tag;
import co.agendaapp.group.GroupScreen;
import co.agendaapp.helpers.CalendarHelper;

import co.agendaapp.R;
import co.agendaapp.model.Group;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.service.HttpRequestTask;
import co.agendaapp.adapter.CalendarEntryListAdapter;
import co.agendaapp.utils.DateTimeSerializer;

import static co.agendaapp.constants.BundleConstants.CALENDAR_ENTRY;
import static co.agendaapp.constants.BundleConstants.CREATE_OP;
import static co.agendaapp.constants.BundleConstants.EDIT_OP;
import static co.agendaapp.constants.BundleConstants.OP;
import static co.agendaapp.helpers.BundleHelper.getSelectedGroup;
import static co.agendaapp.constants.ServiceConstants.CALENDAR_URI;

public class CalendarScreen extends Fragment implements GroupScreen.ResultListener, AdapterView
		.OnItemLongClickListener, RobotoCalendarView.RobotoCalendarListener {

	private Group selectedGroup;
	private ListView listView;
	private List<CalendarEntry> calendarEntries;
	private SwipeRefreshLayout swipeRefreshLayout;
	private CalendarEntry longClickedCalendarEntry;
	private RobotoCalendarView calendarView;
	private DateTime selectedDay;
	private Calendar calendar;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle
			savedInstanceState) {
		calendarEntries = new LinkedList<>();
		return inflater.inflate(R.layout.activity_calendar, container, false);
	}

	@Override
	public void onViewCreated(View view, Bundle savedInstanceState) {
		super.onViewCreated(view, savedInstanceState);

		selectedGroup = getSelectedGroup(getArguments());

		this.calendar = Calendar.getInstance();

		String today = "Today: " + CalendarHelper.formatDate(this.calendar);

		((TextView) view.findViewById(R.id.todayDate)).setText(today);

		this.calendarView = (RobotoCalendarView) view.findViewById(R.id.calendarView);
		this.calendarView.setRobotoCalendarListener(this);
		this.calendarView.markDayAsSelectedDay(this.calendar.getTime());
		this.selectedDay = new DateTime(this.calendar.getTimeInMillis());

		final FloatingActionButton createEntryButton = (FloatingActionButton) view.findViewById(R.id
				.createCalednarEntryFab);
		createEntryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
                getArguments().putString(OP, CREATE_OP);

                openCreateEditEntry();
			}
		});

		this.listView = (ListView) view.findViewById(R.id.calendarEntryList);
		this.listView.setLongClickable(true);
		this.listView.setOnItemLongClickListener(this);
		registerForContextMenu(this.listView);

		this.swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.calendarSwipeContainer);
		swipeRefreshLayout.setColorSchemeColors(Color.BLUE);
		swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
			@Override
			public void onRefresh() {
				getEntries();
			}
		});
	}

	@Override
	public void onStart() {
		super.onStart();

		getEntries();
	}

	private void getEntries() {
		final String URI = CALENDAR_URI + "/getEntries/" + selectedGroup.getGroupId();

		new HttpRequestTask(URI, HttpMethod.GET, getActivity()).execute();
	}

	private void deleteSelectedEntry() {
		final String URI = CALENDAR_URI + "/deleteEntry/" + longClickedCalendarEntry.getEntryId();

		new HttpRequestTask(URI, HttpMethod.DELETE, getActivity()).execute();

		getEntries();
	}

	private void openCreateEditEntry() {
		final Intent createEntryScreen = new Intent(getContext(), CreateEditEntryScreen.class);

		createEntryScreen.putExtras(getArguments());

		startActivity(createEntryScreen);
	}

	@Override
	public void onDateSelected(Date date) {
		calendarView.markDayAsSelectedDay(date);

		long selectedTmp = date.getTime();
		this.selectedDay = new DateTime(selectedTmp);

		populateList(getContext());
	}

	@Override
	public void onLeftButtonClick() {
		this.selectedDay = this.selectedDay.minusMonths(1);

		refreshCalendar();
	}

	@Override
	public void onRightButtonClick() {
		this.selectedDay = this.selectedDay.plusMonths(1);

		refreshCalendar();
	}

	private void refreshCalendar() {
		long millis = this.selectedDay.getMillis();
		this.calendar = Calendar.getInstance();
		this.calendar.setTimeInMillis(millis);

		calendarView.initializeCalendar(this.calendar);
		calendarView.markDayAsSelectedDay(new Date(this.selectedDay.getMillis()));

		for(CalendarEntry calendarEntry : calendarEntries) {
			DateTime entry = calendarEntry.getEntryTimestamp();

			if(inMonth(entry, selectedDay)) {
				markDay(entry);
			}
		}

		populateList(getContext());
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Object value, Context context) {
		this.calendarEntries = (List<CalendarEntry>) value;

		refreshCalendar();

		this.swipeRefreshLayout.setRefreshing(false);
	}

	private void populateList(Context context) {
		List<CalendarEntry> calendarEntryList = new LinkedList<>();

		for(CalendarEntry calendarEntry : calendarEntries) {
			DateTime entry = calendarEntry.getEntryTimestamp();

			if(isDay(entry, this.selectedDay)) {
				calendarEntryList.add(calendarEntry);
			}
		}

		if(calendarEntryList.isEmpty()) {
			CalendarEntry calendarEntry = new CalendarEntry();

			calendarEntry.setTitle("No Entries");
			calendarEntry.setEntryTimestamp(selectedDay);

			calendarEntryList.add(calendarEntry);

			listView.setClickable(false);
		}

		ArrayAdapter<CalendarEntry> adapter = new CalendarEntryListAdapter(context, calendarEntryList);

		listView.setAdapter(adapter);

		adapter.notifyDataSetChanged();
	}

	private void markDay(DateTime day) {
		Date date = new Date(day.getMillis());

		Log.i(Tag.TAG, "Marking: " + date);
		calendarView.markFirstUnderlineWithStyle(R.color.colorPrimary, date);
	}

	private boolean isDay(DateTime x, DateTime y) {
		int dayX = x.getDayOfMonth();
		int dayY = y.getDayOfMonth();

		return inMonth(x, y) && dayX == dayY;
	}

	private boolean inMonth(DateTime x, DateTime y) {
		int yearX = x.getYear();
		int monthX = x.getMonthOfYear();

		int yearY = y.getYear();
		int monthY = y.getMonthOfYear();

		return yearX == yearY && monthX == monthY;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if(v.getId() == R.id.calendarEntryList) {
			MenuInflater inflater = getActivity().getMenuInflater();

			inflater.inflate(R.menu.menu_entries, menu);
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.editEntry:
                try {
                    ObjectMapper mapper = new ObjectMapper();
                    SimpleModule module = new SimpleModule();

                    module.addSerializer(new DateTimeSerializer());
                    mapper.registerModule(module);

                    String selectedCalendarEntry = mapper.writeValueAsString(this.longClickedCalendarEntry);

                    getArguments().putString(CALENDAR_ENTRY, selectedCalendarEntry);

                    getArguments().putString(OP, EDIT_OP);
                } catch(JsonProcessingException e) {
                    Log.e(Tag.TAG, "Issue serializing long clicked calendar entry");
                }

                openCreateEditEntry();

				return true;
			case R.id.deleteEntry:
				Dialog deleteDialog = openDeleteEntryDialog();

				deleteDialog.show();
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}

	@Override
	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
		longClickedCalendarEntry = (CalendarEntry) parent.getItemAtPosition(position);

		Log.i(Tag.TAG, longClickedCalendarEntry.toString());

		return false;
	}

	private Dialog openDeleteEntryDialog() {
		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

		final TextView input = new TextView(getActivity().getApplicationContext());

		input.setTextColor(Color.BLACK);
		input.setInputType(InputType.TYPE_CLASS_TEXT);
		input.setText("Are you sure?");
		builder.setView(input);

		builder.setPositiveButton("Confirm", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				deleteSelectedEntry();
			}
		}).setNegativeButton("Cancal", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});

		return builder.create();
	}
}