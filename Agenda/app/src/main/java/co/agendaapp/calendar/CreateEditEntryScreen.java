package co.agendaapp.calendar;

import android.os.Bundle;
import android.view.View;
import android.widget.Toast;
import android.app.Activity;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.DatePicker;
import android.widget.TimePicker;
import android.widget.AdapterView;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;

import org.joda.time.DateTime;

import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Calendar;

import co.agendaapp.R;
import co.agendaapp.model.Group;
import co.agendaapp.cache.CacheHelper;
import co.agendaapp.model.CalendarEntry;
import co.agendaapp.cache.CacheFileNames;

import static co.agendaapp.constants.BundleConstants.OP;
import static co.agendaapp.constants.BundleConstants.EDIT_OP;
import static co.agendaapp.constants.BundleConstants.CREATE_OP;
import static co.agendaapp.constants.ServiceConstants.ENTRY_CREATED_SUCCESS;
import static co.agendaapp.constants.ServiceConstants.ENTRY_UPDATED_SUCCESS;

import static co.agendaapp.constants.ServiceConstants.NET_ERR;

import static co.agendaapp.helpers.BundleHelper.getSelectedGroup;
import static co.agendaapp.helpers.BundleHelper.getSelectedCalendarEntry;

import co.agendaapp.model.User;
import co.agendaapp.helpers.CalendarHelper;
import co.agendaapp.service.HttpRequestTask;

import static android.widget.Toast.LENGTH_SHORT;

/**
 * Screen to create a calendar entry
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-01
 */
public class CreateEditEntryScreen extends Activity implements HttpRequestTask.ResultListener, AdapterView.OnItemSelectedListener {
	private final Calendar today = Calendar.getInstance();
	private final Calendar entryDate = Calendar.getInstance();
	private final Calendar reminderDate = Calendar.getInstance();

	private Integer reminderHour = 0;
	private Integer reminderMinutes = 0;

	private String selectedRecur;

	private TextView entryDateField;

	private TextView reminderDateField;
	private TextView reminderTimeField;

	private Group selectedGroup;
    private CalendarEntry selectedCalendarEntry;

    private String operation;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		setContentView(R.layout.activity_create_entry_calendar);

        Bundle extras = getIntent().getExtras();

		selectedGroup = getSelectedGroup(extras);
        this.operation = (String) extras.get(OP);

        if(this.operation != null && operation.equals(EDIT_OP)) {
            this.selectedCalendarEntry = getSelectedCalendarEntry(extras);

            String title = EDIT_OP + " Entry";

            ((TextView) findViewById(R.id.createEntryTitle)).setText(title);
        } else {
            this.selectedCalendarEntry = null;
            this.operation = CREATE_OP;

            String title = CREATE_OP + " Entry";

            ((TextView) findViewById(R.id.createEntryTitle)).setText(title);
        }

        if(this.selectedCalendarEntry != null) {
            String title = this.selectedCalendarEntry.getTitle();
            String description = this.selectedCalendarEntry.getDescription();
            DateTime entryTimestamp = this.selectedCalendarEntry.getEntryTimestamp();
            DateTime reminderTimestamp = this.selectedCalendarEntry.getReminderTimestamp();

            ((EditText) findViewById(R.id.titleField)).setText(title);

            if(description != null) {
                ((EditText) findViewById(R.id.descriptionField)).setText(description);
            }

            this.selectedRecur = getSelectedRecur();

            final Spinner recurSpinner = (Spinner) findViewById(R.id.recutSpinner);
            recurSpinner.setSelection(getSelectedPosition());
            this.entryDate.setTimeInMillis(entryTimestamp.getMillis());

            if(reminderTimestamp != null) {
                this.reminderDate.setTimeInMillis(reminderTimestamp.getMillis());

                Calendar temp = Calendar.getInstance();
                temp.setTimeInMillis(reminderTimestamp.getMillis());

                String date = CalendarHelper.formatDate(temp);

                Integer hour = reminderTimestamp.getHourOfDay();
                Integer min = reminderTimestamp.getMinuteOfHour();

                String time = CalendarHelper.formatTime(hour, min);

                ((TextView) findViewById(R.id.reminderDateField)).setText(date);
                ((TextView) findViewById(R.id.reminderTimeField)).setText(time);
            }
        }
	}

	@Override
	protected void onStart() {
		super.onStart();

		initialiseListeners();

		final Button createEntryButton = (Button) findViewById(R.id.createEntryButton);
		createEntryButton.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if(checkBlanks()) {
					createEditEntry();
				}
			}
		});
	}

	private void createEditEntry() {
        final String URI = operation.equals(CREATE_OP) ? "/calendar/createEntry" : "/calendar/updateEntry";
        final HttpMethod method = operation.equals(CREATE_OP) ? HttpMethod.POST : HttpMethod.PUT;

		User user = (User) CacheHelper.read(getCacheDir(), CacheFileNames.USER_CACHE_NAME, User.class);

		String title = getEditFieldText(R.id.titleField);
		String description = getEditFieldText(R.id.descriptionField);

		Integer recurCount = getRecurCount();

		String tempEntry = getTextFieldText(R.id.entryDateField);
		String tempDateReminder = getTextFieldText(R.id.reminderDateField);
		String tempTimeReminder = getTextFieldText(R.id.reminderTimeField);

		DateTime reminderDateTime = null;
		DateTime entryDateTime = CalendarHelper.formatDateTime("00:00", tempEntry);

		if(!tempDateReminder.equals("") || !tempDateReminder.equals("")) {
			reminderDateTime = CalendarHelper.formatDateTime(tempTimeReminder, tempDateReminder);
		}

		CalendarEntry calendarEntry = new CalendarEntry();

		calendarEntry.setGroupId(selectedGroup.getGroupId());
		calendarEntry.setUserId(user.getUserId());
		calendarEntry.setTitle(title);
		calendarEntry.setDescription(description);
		calendarEntry.setRecurCount(recurCount);
		calendarEntry.setEntryTimestamp(entryDateTime);

		if(reminderDateTime != null) {
			calendarEntry.setReminderTimestamp(new DateTime(reminderDateTime.getMillis()));
		}

        if(this.selectedCalendarEntry != null) {
            calendarEntry.setEntryId(this.selectedCalendarEntry.getEntryId());
            calendarEntry.setInsertTimestamp(this.selectedCalendarEntry.getInsertTimestamp());
            calendarEntry.setUpdateTimestamp(this.selectedCalendarEntry.getUpdateTimestamp());
        }

		new HttpRequestTask(URI, method, CreateEditEntryScreen.this).execute(calendarEntry);
	}

	/* Look at fixing this */
	private void initialiseListeners() {
		final Spinner recurSpinner = (Spinner) findViewById(R.id.recutSpinner);
		recurSpinner.setOnItemSelectedListener(this);

		entryDateField = (TextView) findViewById(R.id.entryDateField);

		String date = CalendarHelper.formatDate(entryDate);
		entryDateField.setText(date);

		reminderDateField = (TextView) findViewById(R.id.reminderDateField);
		reminderTimeField = (TextView) findViewById(R.id.reminderTimeField);

		entryDateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CreateEditEntryScreen.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        entryDate.set(year, monthOfYear, dayOfMonth);

                        String date = CalendarHelper.formatDate(entryDate);

                        entryDateField.setText(date);
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

		reminderDateField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new DatePickerDialog(CreateEditEntryScreen.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        reminderDate.set(year, monthOfYear, dayOfMonth);

                        String date = CalendarHelper.formatDate(reminderDate);

                        reminderDateField.setText(date);
                    }
                }, today.get(Calendar.YEAR), today.get(Calendar.MONTH), today.get(Calendar.DAY_OF_MONTH))
                        .show();
            }
        });

		reminderTimeField.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new TimePickerDialog(CreateEditEntryScreen.this, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                        reminderHour = hourOfDay;
                        reminderMinutes = minute;

                        String time = CalendarHelper.formatTime(reminderHour, reminderMinutes);

                        reminderTimeField.setText(time);
                    }
                }, Calendar.HOUR_OF_DAY, Calendar.MINUTE, true).show();
            }
        });
	}

	private boolean checkBlanks() {
		String title = ((EditText) findViewById(R.id.titleField)).getText().toString();

		if(title.equals("")) {
			Toast.makeText(getApplicationContext(), "title cannot be blank", LENGTH_SHORT).show();

			return false;
		}

		return true;
	}

	@Override
	@SuppressWarnings("unchecked")
	public void onResultReceived(Activity activity, Object value, String url) {
		ResponseEntity<Object> responseEntity = (ResponseEntity<Object>) value;
		HttpStatus status = responseEntity.getStatusCode();

		if(status == HttpStatus.CREATED || status == HttpStatus.OK) {
            final String MESSAGE = operation.equals(CREATE_OP) ? ENTRY_CREATED_SUCCESS : ENTRY_UPDATED_SUCCESS;

			Toast.makeText(getApplicationContext(), MESSAGE, LENGTH_SHORT).show();

            finish();
		} else {
            Toast.makeText(getApplicationContext(), NET_ERR, LENGTH_SHORT).show();
		}
	}

	@Override
	public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
		this.selectedRecur = (String) parent.getItemAtPosition(position);
	}

	@Override
	public void onNothingSelected(AdapterView<?> parent) {
		//do nothing
	}

	private Integer getRecurCount() {
		if(selectedRecur.equals("Daily")) {
			return 1;
		}

		if(selectedRecur.equals("Weekly")) {
			return 7;
		}

		if(selectedRecur.equals("Bi-Weekly")) {
			return 14;
		}

		if(selectedRecur.equals("Monthly")) {
			return 30;
		}

		if(selectedRecur.equals("Yearly")) {
			return 365;
		}

		return 0;
	}

    private String getSelectedRecur() {
        Integer tempSelectedRecur = this.selectedCalendarEntry.getRecurCount();

        if(tempSelectedRecur == 1) {
            return "Daily";
        }

        if(tempSelectedRecur == 7) {
            return "Weekly";
        }

        if(tempSelectedRecur == 14) {
            return "Bi-Weekly";
        }

        if(tempSelectedRecur == 30) {
            return "Monthly";
        }

        if(tempSelectedRecur == 365) {
            return "Yearly";
        }

        return "Don't";
    }

    private Integer getSelectedPosition() {
        if(this.selectedRecur.equals("Daily")) {
            return 1;
        }

        if(this.selectedRecur.equals("Weekly")) {
            return 2;
        }

        if(this.selectedRecur.equals("Bi-Weekly")) {
            return 3;
        }

        if(this.selectedRecur.equals("Monthly")) {
            return 4;
        }

        if(this.selectedRecur.equals("Yearly")) {
            return 5;
        }

        return 0;
    }

	private String getEditFieldText(int resId) {
		return ((EditText) findViewById(resId)).getText().toString().trim();
	}

	private String getTextFieldText(int resId) {
		return ((TextView) findViewById(resId)).getText().toString().trim();
	}
}