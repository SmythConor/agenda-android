package co.agendaapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import co.agendaapp.R;
import co.agendaapp.model.Group;

/**
 * Group list adapter for displaying a group
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-30
 */
public class GroupListAdapter extends ArrayAdapter<Group> {
    private final Context context;
    private final List<Group> values;

    public GroupListAdapter(Context context, List<Group> values) {
        super(context, android.R.layout.simple_list_item_1, values);
        this.context = context;
        this.values = values;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View rowView = convertView;

        if(rowView == null) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            rowView = inflater.inflate(R.layout.group_list_item, parent, false);
        }

        TextView textView = (TextView) rowView.findViewById(R.id.text);

        if(textView != null) {
            String name = values.get(position).getName();

            textView.setText(name);
        }

        return rowView;
    }
}