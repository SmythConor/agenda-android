package co.agendaapp.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.DateTime;

import java.util.List;

import co.agendaapp.R;
import co.agendaapp.model.CalendarEntry;

/**
 * List adapter for calendar entries
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-13
 */
public class CalendarEntryListAdapter extends ArrayAdapter<CalendarEntry> {
	private Context context;
	private List<CalendarEntry> values;

	public CalendarEntryListAdapter(Context context, List<CalendarEntry> values) {
		super(context, android.R.layout.simple_list_item_1, values);

		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if(rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

			rowView = inflater.inflate(R.layout.calendar_entry_list_item, parent, false);
		}

		TextView titleView = (TextView) rowView.findViewById(R.id.title);
		TextView descriptionView = (TextView) rowView.findViewById(R.id.decription);

		if(titleView != null) {
			String titleValue = values.get(position).getTitle();

			titleView.setText(titleValue);
		} if(descriptionView != null) {
			String description = values.get(position).getDescription();

			descriptionView.setText(description);
		}

		return rowView;
	}
}