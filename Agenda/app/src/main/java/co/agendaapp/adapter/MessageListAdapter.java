package co.agendaapp.adapter;

import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;

import java.util.List;

import co.agendaapp.R;
import co.agendaapp.helpers.CalendarHelper;
import co.agendaapp.model.Message;
import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;

/**
 * For displaying messages
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-23
 */
public class MessageListAdapter extends ArrayAdapter<Message> {
	private Context context;
	private List<Message> values;

	public MessageListAdapter(Context context, List<Message> values) {
		super(context, android.R.layout.simple_list_item_1, values);

		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if(rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
					.LAYOUT_INFLATER_SERVICE);

			rowView = inflater.inflate(R.layout.message_list_item, parent, false);
		}

		TextView messageView = (TextView) rowView.findViewById(R.id.messageContent);
		TextView authorView = (TextView) rowView.findViewById(R.id.authorField);
        TextView timestampView = (TextView) rowView.findViewById(R.id.timestampField);

		if(messageView != null) {
			String messageValue = values.get(position).getMessageBody();

			messageView.setText(messageValue);
		}

		if(authorView != null) {
			String authorValue = "From: " + values.get(position).getSenderId();

			authorView.setText(authorValue);
		}

        if(timestampView != null) {
            DateTime sentTimestamp = values.get(position).getSentTimestamp();

            String year = String.valueOf(sentTimestamp.get(DateTimeFieldType.year()));
            String month = String.valueOf(sentTimestamp.get(DateTimeFieldType.monthOfYear()));
            String day = String.valueOf(sentTimestamp.get(DateTimeFieldType.dayOfMonth()));

            String hour = String.valueOf(sentTimestamp.get(DateTimeFieldType.clockhourOfDay()));
            String mins = String.valueOf(sentTimestamp.get(DateTimeFieldType.minuteOfHour()));

            String timestamp = " Sent: " + year + "-" + month + "-" + day + " at " + hour + ":" + mins;

            timestampView.setText(timestamp);
        }

		return rowView;
	}
}