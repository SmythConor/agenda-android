package co.agendaapp.adapter;

import android.content.res.Resources;
import android.view.View;
import android.view.ViewGroup;
import android.content.Context;
import android.widget.TextView;
import android.view.LayoutInflater;
import android.widget.ArrayAdapter;

import java.util.List;

import co.agendaapp.R;
import co.agendaapp.model.Priority;
import co.agendaapp.model.ToDoListEntry;

/**
 * List adapter for the to list
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-05-03
 */
@SuppressWarnings("deprecation")
public class ToDoListEntryAdapter extends ArrayAdapter<ToDoListEntry> {
	private Context context;
	private List<ToDoListEntry> values;

	public ToDoListEntryAdapter(Context context, List<ToDoListEntry> values) {
		super(context, android.R.layout.simple_list_item_1, values);

		this.context = context;
		this.values = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View rowView = convertView;

		if(rowView == null) {
			LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context
					.LAYOUT_INFLATER_SERVICE);

			rowView = inflater.inflate(R.layout.to_do_list_item, parent, false);
		}

		TextView priorityView = (TextView) rowView.findViewById(R.id.priority);
		TextView titleView = (TextView) rowView.findViewById(R.id.titleField);
		TextView descriptionView = (TextView) rowView.findViewById(R.id.descriptionField);

		if(priorityView != null) {
			Priority priority = values.get(position).getPriority();

			String strPriority = "";
			Resources resources = context.getResources();

			if(priority == Priority.HIGH) {
				strPriority = "H";
				priorityView.setBackgroundColor(resources.getColor(R.color.highPriority));
			} else if(priority == Priority.MEDIUM) {
				strPriority = "M";
				priorityView.setBackgroundColor(resources.getColor(R.color.mediumPriority));
			} else if(priority == Priority.LOW) {
				strPriority = "L";
				priorityView.setBackgroundColor(resources.getColor(R.color.lowPriority));
			}

			priorityView.setText(strPriority);
		}
		if(titleView != null) {
			String titleValue = values.get(position).getTitle();

			titleView.setText(titleValue);
		}
		if(descriptionView != null) {
			String description = values.get(position).getDescription();

			descriptionView.setText(description);
		}

		return rowView;
	}
}