package co.agendaapp.adapter;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import co.agendaapp.group.GroupScreen;
import co.agendaapp.calendar.CalendarScreen;
import co.agendaapp.todolist.ToDoListScreen;
import co.agendaapp.messaging.MessagingScreen;

/**
 * Fragment tab page manager
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-01
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {
	private Bundle extras;
	private GroupScreen.ResultListener[] notifiers;

	public TabsPagerAdapter(FragmentManager fragmentManager, Bundle extras, GroupScreen
			.ResultListener[] notifiers) {
		super(fragmentManager);

		this.extras = extras;
		this.notifiers = notifiers;
	}

	@Override
	public Fragment getItem(int index) {
		switch(index) {
			case 0:
				Fragment calendarScreen = new CalendarScreen();

				calendarScreen.setArguments(extras);
				notifiers[index] = (GroupScreen.ResultListener) calendarScreen;

				return calendarScreen;
			case 1:
				Fragment messagingScreen = new MessagingScreen();

				messagingScreen.setArguments(extras);
				notifiers[index] = (GroupScreen.ResultListener) messagingScreen;

				return messagingScreen;
			case 2:
				Fragment toDoListScreen = new ToDoListScreen();

				toDoListScreen.setArguments(extras);
				notifiers[index] = (GroupScreen.ResultListener) toDoListScreen;

				return toDoListScreen;
		}

		return null;
	}

	@Override
	public int getCount() {
		return 3;
	}
}