package co.agendaapp.encrypt;

import org.junit.Test;

import static java.nio.charset.StandardCharsets.UTF_8;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
/**
 * Unit tests for the encryption class
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-03-06
 */
public class EncryptorTest {

    @Test
    public void getSaltTest() {
        byte[] salt = EncryptionUtils.getSalt();

        assertNotNull(salt);
    }

    @Test
    public void hashPassworTest() {
        String password = "test-password";

        byte[] bytePassword = password.getBytes(UTF_8);
        byte[] salt = EncryptionUtils.getSalt();

        byte[] concatedArrays = concatArray(bytePassword, salt);
        String before = new String(concatedArrays, UTF_8);

        String after = Encryptor.hashPassword(bytePassword, salt);

        assertNotEquals(before, after);
    }

    private static byte[] concatArray(byte[] a, byte[] b) {
        byte[] c = new byte[a.length + b.length];

        System.arraycopy(a, 0, c, 0, a.length);
        System.arraycopy(b, 0, c, a.length, b.length);

        return c;
    }
}