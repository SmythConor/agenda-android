package co.agendaapp.helpers;

import org.joda.time.DateTime;
import org.junit.Test;

import java.sql.Date;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

import co.agendaapp.encrypt.EncryptionUtils;
import co.agendaapp.encrypt.Encryptor;
import co.agendaapp.model.ToDoListEntry;

import static java.nio.charset.StandardCharsets.UTF_8;
import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

/**
 * {Insert Description}
 *
 * @author Conor Smyth <cnrsmyth@gmail.com>
 * @version 0.1
 * @since 2016-04-05
 */
public class CalendarHelperTest {

    @Test
    public void formatDateTimeTest() {
        String time = "23:00";
        String date = "2016-Apr-23";

        DateTime dateTime = CalendarHelper.formatDateTime(time, date);
        System.out.println(dateTime);

        assertNotNull(dateTime);
    }
}
